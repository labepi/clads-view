/**
 * Copyright (C) 2010-2012 Joao Paulo de Souza Medeiros
 *
 * Author(s): Joao Paulo de Souza Medeiros <ignotus21@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "graph.h"
#include <string.h>
#include <time.h> 

static clads_bool_type line_intersection(cartesian_coordinates* p0, cartesian_coordinates* p1, 
    cartesian_coordinates* p2, cartesian_coordinates* p3){
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1->x - p0->x;     
    s1_y = p1->y - p0->y;
    s2_x = p3->x - p2->x;     
    s2_y = p3->y - p2->y;

    float s, t;
    s = (-s1_y * (p0->x - p2->x) + s1_x * (p0->y - p2->y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0->y - p2->y) - s2_y * (p0->x - p2->x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        return 1;
    }

    return 0; 
}

int clads_get_line_crossings(clads_graph_type *g){
    int count = 0;
    clads_list_node_type *list_inter1 = g->l_edge->head;
    clads_list_node_type *list_inter2 = g->l_edge->head;
    
    clads_graph_edge_type *e;
    clads_graph_edge_type *e2;
   
    clads_graph_node_type *na;
    clads_graph_node_type *nb;
    clads_graph_node_type *na2;
    clads_graph_node_type *nb2;

    cartesian_coordinates* na_coordinates;
    cartesian_coordinates* nb_coordinates;
    cartesian_coordinates* na2_coordinates;
    cartesian_coordinates* nb2_coordinates;
    while (list_inter1 != NULL)
    {   
        e = CLADS_CAST(list_inter1->info,clads_graph_edge_type*);
        na = CLADS_CAST(e->na,clads_graph_node_type*);
        nb = CLADS_CAST(e->nb,clads_graph_node_type*);
        list_inter2 = list_inter1->next;
        while (list_inter2 != NULL)
        {   
            e2 = CLADS_CAST(list_inter2->info,clads_graph_edge_type*);
            na2 = CLADS_CAST(e2->na,clads_graph_node_type*);
            nb2 = CLADS_CAST(e2->nb,clads_graph_node_type*);
            if ((na->id != na2->id) && (na->id != nb2->id) &&(nb->id != na2->id) && (nb->id != nb2->id)){
                na_coordinates = CLADS_CAST(na->internal_info,cartesian_coordinates*);
                nb_coordinates = CLADS_CAST(nb->internal_info,cartesian_coordinates*);
                na2_coordinates = CLADS_CAST(na2->internal_info,cartesian_coordinates*);
                nb2_coordinates = CLADS_CAST(nb2->internal_info,cartesian_coordinates*);
                if (line_intersection(na_coordinates,nb_coordinates,na2_coordinates,
                    nb2_coordinates)){
                    count++;
                }
            }
           
            list_inter2 = list_inter2->next;
        }
        list_inter1 = list_inter1->next;
    }
    return count;
}

//TODO: refactor
static clads_real_type calc_delta_x(cartesian_coordinates* coordinates_1, cartesian_coordinates* coordinates_2){
        return (coordinates_1->x - coordinates_2->x);
}

static clads_real_type calc_delta_y(cartesian_coordinates* coordinates_1, cartesian_coordinates* coordinates_2){
        return (coordinates_1->y - coordinates_2->y);
}

static float calc_distance(clads_graph_node_type* node1, clads_graph_node_type* node2){
        cartesian_coordinates *coordinates_1 = CLADS_CAST(node1->internal_info,cartesian_coordinates*); 
        cartesian_coordinates *coordinates_2 = CLADS_CAST(node2->internal_info,cartesian_coordinates*); 
        clads_real_type delta_x = calc_delta_x(coordinates_1,coordinates_2);
        clads_real_type delta_y = calc_delta_y(coordinates_1,coordinates_2);
        return hypot(delta_x, delta_y);
}
static float calc_angle(clads_graph_node_type* node1, clads_graph_node_type* node2){
        cartesian_coordinates *coordinates_1 = CLADS_CAST(node1->internal_info,cartesian_coordinates*); 
        cartesian_coordinates *coordinates_2 = CLADS_CAST(node2->internal_info,cartesian_coordinates*); 
        clads_real_type delta_x = calc_delta_x(coordinates_1,coordinates_2);
        clads_real_type delta_y = calc_delta_y(coordinates_1,coordinates_2);
        return atan2(delta_y, delta_x);
}

clads_order_type
clads_graph_default_f_compare(clads_graph_type *g,
                              clads_addr_type a,
                              clads_addr_type b)
{
    /*
     * As default threat as integer values.
     */
    if (*((int *) a) == *((int *) b))
        return clads_equal;
    if (*((int *) a) < *((int *) b))
        return clads_less;
    return clads_more;
}

clads_addr_type
clads_graph_edge_f_copy(clads_addr_type a)
{
    clads_graph_edge_type *f = CLADS_ALLOC(1, clads_graph_edge_type);
    clads_graph_edge_type *e = CLADS_CAST(a, clads_graph_edge_type *);

    f->na = e->na;
    f->nb = e->nb;
    f->info = e->info;
    f->key = e->key;

    return CLADS_CAST(f, clads_addr_type);
}

clads_addr_type
clads_graph_node_f_copy(clads_addr_type a)
{
    clads_graph_node_type *m = CLADS_ALLOC(1, clads_graph_node_type);
    clads_graph_node_type *n = CLADS_CAST(a, clads_graph_node_type *);

    m->id = n->id;
    m->clustering = n->clustering;
    m->info = n->info;
    m->key = n->key;

    return CLADS_CAST(m, clads_addr_type);
}

clads_void_type
clads_graph_initialize(clads_graph_type *g)
{
    g->n_node = 0;
    g->n_edge = 0;
    g->l_node = CLADS_ALLOC(1, clads_list_type);
    g->l_edge = CLADS_ALLOC(1, clads_list_type);
    g->allow_loop = clads_false;
    g->allow_multiple_edges = clads_false;
    g->is_directed = clads_false;
    g->l_adjacency = NULL;
    g->f_compare = &clads_graph_default_f_compare;
    g->more = NULL;

    clads_list_initialize(g->l_node);
    g->l_node->f_copy = &clads_graph_node_f_copy;
    g->l_node->do_free_info = clads_false;
    clads_list_initialize(g->l_edge);
    g->l_edge->f_copy = &clads_graph_edge_f_copy;
    g->l_edge->do_free_info = clads_false;
}

clads_graph_type *
clads_graph_copy(clads_graph_type *g)
{
    clads_graph_type *ng = CLADS_ALLOC(1, clads_graph_type);

    ng->n_node = g->n_node;
    ng->n_edge = g->n_edge;
    ng->allow_loop = g->allow_loop;
    ng->allow_multiple_edges = g->allow_multiple_edges;
    ng->is_directed = g->is_directed;
    ng->f_compare = g->f_compare;
    ng->more = g->more;

    ng->l_edge = clads_list_copy(g->l_edge);
    ng->l_node = clads_list_copy(g->l_node);

    if (g->l_adjacency != NULL)
        clads_graph_mount_adjacency(ng);
    else
        ng->l_adjacency = NULL;

    return ng;
}

static clads_void_type
clads_graph_free_node_info(clads_graph_type *g){
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;
    while (l != NULL)
    {   
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        if (n->info != NULL){
            CLADS_FREE(n->info);
        }
        l = l->next;
    }
}
clads_void_type
clads_graph_finalize(clads_graph_type *g)
{
    if (g != NULL)
    {
        g->l_node->do_free_info = clads_true;
        g->l_edge->do_free_info = clads_true;
        clads_graph_free_node_info(g);
        clads_list_finalize(g->l_edge);
        CLADS_FREE(g->l_edge);
        clads_list_finalize(g->l_node);
        CLADS_FREE(g->l_node);
        clads_graph_clear_adjacency(g);
    }
#if CLADS_DEBUG
    else
        printf("W. [GRAPH] Trying to finalize a NULL pointer.\n");
#endif
}

clads_graph_type *
clads_graph_new_erdos_nm(clads_size_type n,
                         clads_size_type m,
                         clads_bool_type is_directed,
                         clads_bool_type allow_loop,
                         clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_graph_node_type *na, *nb;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating nodes.
     */
    while (g->n_node < n)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating edges.
     */
    while (g->n_edge < m)
    {
        na = v[clads_randint(0, n - 1)];
        nb = v[clads_randint(0, n - 1)];

        clads_graph_add_edge(g, na, nb, NULL);
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_type *
clads_graph_new_erdos_np(clads_size_type n,
                         clads_real_type p,
                         clads_bool_type is_directed,
                         clads_bool_type allow_loop,
                         clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_size_type i, j, start;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating nodes.
     */
    while (g->n_node < n)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating edges.
     */
    for (i = 0; i < g->n_node; i++)
    {
        start = (g->is_directed == clads_true) ? 0 : i;

        for (j = start; j < g->n_node; j++)
        {
            if (clads_statistic_uniform_trial(p))
                clads_graph_add_edge(g, v[i], v[j], NULL);
        }
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_type *
clads_graph_new_watts(clads_size_type n,
                      clads_size_type k,
                      clads_real_type p,
                      clads_bool_type is_directed,
                      clads_bool_type allow_loop,
                      clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type *r, **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_graph_edge_type *e;
    clads_list_node_type *l;
    clads_size_type i, j;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating nodes.
     */
    while (g->n_node < n)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating edges (multiple edges and loops are threated by
     * `clads_graph_add_edge' function).
     */
    for (i = 0; i < g->n_node; i++)
    {
        clads_graph_add_edge(g, v[i], v[i], NULL);

        for (j = 1; j <= k / 2; j++)
        {
            clads_graph_add_edge(g, v[i], v[clads_loop_index(i + j, n)], NULL);
            clads_graph_add_edge(g, v[i], v[clads_loop_index(i - j, n)], NULL);
        }
    }

    /*
     * Rewiring edges.
     */
    while ((l = clads_list_next(g->l_edge)))
    {
        e = (clads_graph_edge_type *) l->info;

        if (clads_statistic_uniform_trial(p))
        {
            r = v[clads_randint(0, n - 1)];

            if (clads_statistic_uniform_trial(0.5))
                e->na = r;
            else
                e->nb = r;
        }
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_type *
clads_graph_new_barabasi(clads_size_type n,
                         clads_size_type m,
                         clads_bool_type is_directed,
                         clads_bool_type allow_loop,
                         clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_size_type count, index, drawn;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating the first `m' nodes.
     */
    while (g->n_node < m)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating the remaining `n' - `m' nodes.
     */
    while (g->n_node < n)
    {
        count = 0;
        index = g->n_node;

        v[index] = clads_graph_add_node(g, NULL);

        while (count < m)
        {
            // TODO: probabilistic function.
            drawn = 0;

            if (clads_graph_add_edge(g, v[index], v[drawn], NULL))
                count++;
        }
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_edge_type *
clads_graph_edge_new(clads_void_type)
{
    clads_graph_edge_type *e = CLADS_ALLOC(1, clads_graph_edge_type);

    e->na = NULL;
    e->nb = NULL;
    e->info = NULL;
    e->key = clads_off;

    return e;
}

clads_void_type
clads_graph_clear_adjacency(clads_graph_type *g)
{
    clads_size_type i;

    if (g->l_adjacency != NULL)
    {
        for (i = 0; i < g->n_node; i++)
        {   
            g->l_adjacency[i]->do_free_info = clads_false;
            clads_list_finalize(g->l_adjacency[i]);
            CLADS_FREE(g->l_adjacency[i]);
        }

        CLADS_FREE(g->l_adjacency);
        g->l_adjacency = NULL;
    }
}

clads_void_type
clads_graph_mount_adjacency(clads_graph_type *g)
{
    clads_graph_edge_type *e;
    clads_list_node_type *n, *p;
    clads_size_type i;

    /*
     * Clear the adjacency list if it already exists, and create a new one.
     */
    clads_graph_clear_adjacency(g);

    g->l_adjacency = CLADS_ALLOC(g->n_node, clads_list_type *);

    /*
     * Initializing lists.
     */
    for (i = 0; i < g->n_node; i++)
    {
        g->l_adjacency[i] = CLADS_ALLOC(1, clads_list_type);
        clads_list_initialize(g->l_adjacency[i]);
    }

    /*
     * Fill the adjacency list.
     */
    while ((p = clads_list_next(g->l_edge)))
    {
        e = (clads_graph_edge_type *) p->info;

        n = clads_list_node_new();
        n->info = (clads_addr_type) e->nb;
        clads_list_insert(g->l_adjacency[e->na->id], n);

        if (!g->is_directed)
        {
            n = clads_list_node_new();
            n->info = (clads_addr_type) e->na;
            clads_list_insert(g->l_adjacency[e->nb->id], n);
        }
    }
}

clads_list_type *
clads_graph_get_edges_by_nodes(clads_graph_type *g,
                               clads_graph_node_type *na,
                               clads_graph_node_type *nb)
{
    clads_list_node_type *l = g->l_edge->head;
    clads_graph_edge_type *e;
    clads_list_node_type *p;

    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);
    clads_list_initialize(r);

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if ((e->na == na && e->nb == nb) ||
            (!g->is_directed && e->na == nb && e->nb == na))
        {
            p = clads_list_node_new();
            p->info = e;
            clads_list_insert(r, p);
        }

        l = l->next;
    }

    if (clads_list_is_empty(r) == clads_true)
    {
        clads_list_finalize(r);
        CLADS_FREE(r);
        r = NULL;
    }

    return r;
}

clads_graph_edge_type *
clads_graph_get_edge_by_nodes(clads_graph_type *g,
                              clads_graph_node_type *na,
                              clads_graph_node_type *nb)
{
    clads_list_node_type *l = g->l_edge->head;
    clads_graph_edge_type *e;

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if ((e->na == na && e->nb == nb) ||
            (!g->is_directed && e->na == nb && e->nb == na))
            return e;

        l = l->next;
    }

    return NULL;
}

clads_graph_node_type *
clads_graph_get_node(clads_graph_type *g,
                     clads_id_type id)
{
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;

    while (l != NULL)
    {
        n = (clads_graph_node_type *) l->info;
        if (n->id == id){
            return n;
        }
        l = l->next;
    }
    return NULL;
}

clads_graph_node_type *
clads_graph_get_node_by_info(clads_graph_type *g,
                             clads_addr_type info)
{
    clads_list_node_type *l = g->l_edge->head;
    clads_graph_node_type *n;

    while (l != NULL)
    {
        n = (clads_graph_node_type *) l->info;

        if (g->f_compare(g, n->info, info) == clads_equal)
            return n;

        l = l->next;
    }

    return NULL;
}

clads_graph_edge_type *
clads_graph_add_edge(clads_graph_type *g,
                     clads_graph_node_type *na,
                     clads_graph_node_type *nb,
                     clads_addr_type info)
{
    clads_graph_edge_type *e;
    clads_list_node_type *p;
    if (g->allow_loop == clads_false && na == nb)
        return NULL;

    if (g->allow_multiple_edges == clads_false &&
        clads_graph_get_edge_by_nodes(g, na, nb) != NULL)
        return NULL;

    e = CLADS_ALLOC(1, clads_graph_edge_type);

    e->id = g->n_edge++;

    e->na = na;
    e->nb = nb;
    e->info = info;

    p = clads_list_node_new();
    p->info = (clads_addr_type) e;
    clads_list_insert(g->l_edge, p);

    return e;
}

clads_graph_node_type *
clads_graph_add_node(clads_graph_type *g,
                     clads_addr_type info)
{
    clads_graph_node_type *n = CLADS_ALLOC(1, clads_graph_node_type);
    clads_list_node_type *p;

    n->id = g->n_node++;
    n->info = info;
    p = clads_list_node_new();
    p->info = (clads_addr_type) n;
    clads_list_insert(g->l_node, p);

    return n;
}

clads_graph_node_type *
clads_graph_add_node_with_id(clads_graph_type *g,
                     clads_addr_type info,clads_id_type id)
{   
    clads_graph_node_type *n;
    n = clads_graph_get_node(g,id);
    if (n != NULL ){
        printf("already exists a node with id: %llu\n",id);
        return NULL;
    }
    n = CLADS_ALLOC(1, clads_graph_node_type);
    clads_list_node_type *p;

    n->id = id;
    n->info = info;
    n->degree = 0;
    p = clads_list_node_new();
    p->info = (clads_addr_type) n;
    clads_list_insert(g->l_node, p);
    g->n_node++;
    return n;

}

clads_list_type *
clads_graph_get_edges_by_node(clads_graph_type *g,
                              clads_graph_node_type *n)
{
    clads_list_node_type *p, *l = g->l_edge->head;
    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);
    clads_graph_edge_type *e;

    clads_list_initialize(r);

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if (e->na == n || e->nb == n)
        {
            p = clads_list_node_new();
            p->info = (clads_addr_type) e;
            clads_list_insert(r, p);
        }

        l = l->next;
    }

    return r;
}

clads_list_type *
clads_graph_get_node_neighbors(clads_graph_type *g,
                               clads_graph_node_type *n)
{
    clads_list_node_type *p, *l = g->l_edge->head;
    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);
    clads_graph_edge_type *e;

    clads_list_initialize(r);

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if (e->na == n || e->nb == n)
        {
            p = clads_list_node_new();

            if (e->na == n)
                p->info = (clads_addr_type) e->nb;
            else
                p->info = (clads_addr_type) e->na;

            clads_list_insert(r, p);
        }

        l = l->next;
    }

    return r;
}

clads_real_type
clads_graph_clustering(clads_graph_type *g)
{
    clads_list_node_type *l = g->l_node->head;
    clads_real_type size = g->n_node;
    clads_graph_node_type *n;

    g->clustering = 0;

    while (l != NULL)
    {
        n = (clads_graph_node_type *) l->info;
        g->clustering += clads_graph_node_clustering(g, n);

        l = l->next;
    }

    g->clustering = g->clustering / size;

    return g->clustering;
}

clads_real_type
clads_graph_node_clustering(clads_graph_type *g,
                            clads_graph_node_type *n)
{
    // TODO

    return 0;
}

clads_list_type *
clads_graph_spanning_tree(clads_graph_type *g)
{
    clads_graph_node_type *a, *b;
    clads_graph_edge_type *e;
    clads_list_node_type *l;
    clads_list_type *q = CLADS_ALLOC(1, clads_list_type);
    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);

    clads_list_initialize(q);
    clads_list_initialize(r);

    /*
     * Initialize nodes as unvisited.
     */
    while ((l = clads_list_next(g->l_node)))
    {
        a = CLADS_CAST(l->info, clads_graph_node_type *);
        a->key = clads_off;
    }

    /*
     * Using breadth-first-search to select the edges for the spanning tree.
     */
    clads_graph_mount_adjacency(g);

    a = CLADS_CAST(g->l_node->head->info, clads_graph_node_type *);
    a->key = clads_on;

    l = clads_list_node_new();
    l->info = a;

    clads_list_enqueue(q, l);

    while (clads_list_is_empty(q) == clads_false)
    {
        l = clads_list_dequeue(q);
        a = CLADS_CAST(l->info, clads_graph_node_type *);
        clads_list_node_finalize(l,clads_false);

        /*
         * Iterate over each neighbor of node `a'.
         */
        while ((l = clads_list_next(g->l_adjacency[a->id])))
        {
            b = CLADS_CAST(l, clads_graph_node_type *);

            /*
             * If node `b' not visited yet, do it.
             */
            if (b->key == clads_off)
            {
                b->key = clads_on;

                l = clads_list_node_new();
                l->info = b;
                clads_list_enqueue(q, l);

                e = clads_graph_edge_new();
                e->na = a;
                e->nb = b;
                l = clads_list_node_new();
                l->info = e;
                clads_list_insert(r, l);
            }
        }
    }

    if (clads_list_is_empty(r) == clads_true)
    {
        clads_list_finalize(r);
        CLADS_FREE(r);
        r = NULL;
    }
    CLADS_FREE(q);
    return r;
}

clads_void_type
clads_graph_initialize_from_tgf_file(clads_graph_type *g,char *filename)
{  
    FILE *file = fopen(filename, "r");
    if(file != NULL)
    {       
        clads_graph_initialize(g);
        char line[100];
        clads_id_type id;
        char* label;
        char* label_node;
        int len_label;
        while(strcmp(fgets(line, 100, file),"#\n") != 0){
            id = strtoull(strtok (line," "),NULL,10);
            label = strtok (NULL,"\n");
            if (label != NULL){
                len_label = strlen(label);
                label_node = CLADS_ALLOC(len_label,char);
                strcpy(label_node, label);
                label = label_node;
            }
            clads_graph_add_node_with_id(g,label,id);
        }
        while(fgets(line, 100, file) != NULL){
            clads_graph_node_type* na =  clads_graph_get_node(g,strtoull (strtok (line," "),NULL,10));
            clads_graph_node_type* nb =  clads_graph_get_node(g,strtoull (strtok (NULL," "),NULL,10));
            clads_graph_add_edge(g,na,nb,NULL);
        }
        fclose(file);
    }
   else
   {
      perror(filename);
   }
}


clads_void_type
clads_graph_initialize_force_layout(clads_graph_type *g,clads_real_type max_x, clads_real_type max_y){
    
    clads_list_node_type *l;
    force_layout_node_information *info;
    clads_graph_node_type *n;
    while ((l = clads_list_next(g->l_node)))
    {
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        info = CLADS_ALLOC(1,force_layout_node_information);
        n->internal_info = info;
        info->coordinates.x =  ((clads_real_type)rand() / (clads_real_type)(RAND_MAX / max_x));
        info->coordinates.y =  ((clads_real_type)rand() / (clads_real_type)(RAND_MAX / max_y));
        info->velocity_x = 0;
        info->velocity_y = 0;
        info->charge = 1;
        info->mass = 1;
        info->net_force_x = 0;
        info->net_force_y = 0;
        info->node_neighbors = clads_graph_get_node_neighbors(g,n);
    }
   
}

int
clads_graph_initialize_radial_layout(clads_graph_type *g, clads_real_type radius){
    clads_list_node_type *l;
    radial_layout_node_information *info_a;
    radial_layout_node_information *info_b;
    clads_graph_node_type *n;

    while ((l = clads_list_next(g->l_node)))
    {
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        info_a = CLADS_ALLOC(1,radial_layout_node_information);
        n->internal_info = info_a;
        info_a->coordinates.x =  0;
        info_a->coordinates.y =  0;
        info_a->radius = radius;
        info_a->angle = 0;
        info_a->angular_space = 0;
        info_a->min_angular_space = 0;
        info_a->parent = NULL;
        info_a->children = CLADS_ALLOC(1, clads_list_type);
        clads_list_initialize(info_a->children);
    }

    clads_graph_node_type *a, *b;

    clads_list_type *q = CLADS_ALLOC(1, clads_list_type);

    clads_list_initialize(q);

    while ((l = clads_list_next(g->l_node)))
    {
        a = CLADS_CAST(l->info, clads_graph_node_type *);
        info_a = CLADS_CAST(a->internal_info, radial_layout_node_information*);
        a->key = clads_off;
        info_a->depth = 0;
    }

    clads_graph_mount_adjacency(g);
    
    a = CLADS_CAST(g->l_node->head->info, clads_graph_node_type *);
    a->key = clads_on;

    l = clads_list_node_new();
    l->info = a;

    clads_list_enqueue(q, l);
    int tree_depth = 0;
    clads_list_node_type *node_child;
    while (clads_list_is_empty(q) == clads_false)
    {
        l = clads_list_dequeue(q);
        a = CLADS_CAST(l->info, clads_graph_node_type *);
        info_a = CLADS_CAST(a->internal_info, radial_layout_node_information*);
        clads_list_node_finalize(l,clads_false);
        while ((l = clads_list_next(g->l_adjacency[a->id])))
        {
            b = CLADS_CAST(l->info, clads_graph_node_type *);
            info_b = CLADS_CAST(b->internal_info, radial_layout_node_information*);
            if (b->key == clads_off)
            {
                b->key = clads_on;

                info_b->parent = a;
                node_child = clads_list_node_new();
                node_child->info = b;
                clads_list_insert(info_a->children,node_child);
                info_b->depth = info_a->depth+1;
                if(info_b->depth > tree_depth){
                    tree_depth = info_b->depth;
                }
                l = clads_list_node_new();
                l->info = b;
                clads_list_enqueue(q, l);
            }
        }
    }

    clads_list_finalize(q);
    CLADS_FREE(q);
    return tree_depth;
}

clads_void_type
clads_graph_finalize_radial_layout(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *n;
    radial_layout_node_information *info;
    while ((l = clads_list_next(g->l_node)))
    {      
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        info = CLADS_CAST(n->internal_info,radial_layout_node_information*);
        info->children->do_free_info = clads_false;
        clads_list_finalize(info->children);
        CLADS_FREE(info->children);
        CLADS_FREE(info);
        n->internal_info = NULL;
        
    }
}

static clads_void_type
clads_graph_angular_space(clads_graph_node_type *v,double remaining_angular_space){
    radial_layout_node_information *info_v = CLADS_CAST(v->internal_info,radial_layout_node_information*);
    if (info_v->depth == 0){
        info_v->angular_space = 2*M_PI;
    }
    else{
        radial_layout_node_information *info_parent = CLADS_CAST(info_v->parent->internal_info,radial_layout_node_information*);
        info_v->angular_space = remaining_angular_space*info_v->min_angular_space/info_parent->min_angular_space;
    }

    clads_list_node_type *child;
    clads_graph_node_type *child_node;
    while((child = clads_list_next(info_v->children))){
        child_node = CLADS_CAST(child->info,clads_graph_node_type*);
        clads_graph_angular_space(child_node,info_v->angular_space);
    }

}

static double clads_graph_minimum_angular_space(clads_graph_node_type *v,double base_radius){
    double j = 0;
    double s = 0;
    radial_layout_node_information *iv = CLADS_CAST(v->internal_info,radial_layout_node_information*);
    clads_list_node_type *child;
    clads_graph_node_type *child_node;
    radial_layout_node_information *child_information;
    if (iv->depth != 0){
        j = 2 * asin(iv->radius/(iv->depth*base_radius));
    }
    while((child = clads_list_next(iv->children))){
        child_node = CLADS_CAST(child->info,clads_graph_node_type*);
        child_information = CLADS_CAST(child_node->internal_info,radial_layout_node_information*);
        clads_graph_minimum_angular_space(child_node,base_radius);
        s += child_information->min_angular_space;
    }
    iv->min_angular_space = MAX(j,s);
    return iv->min_angular_space;
   
}

static double clads_graph_get_base_radius(clads_graph_type *g,clads_graph_node_type *root){
    double rc = 0.0;
    double ri;
    double rj;
    clads_list_node_type *l = g->l_edge->head;
    clads_graph_edge_type *e;
    radial_layout_node_information *na_info;
    radial_layout_node_information *nb_info;
    while (l != NULL)
    {   
        e = CLADS_CAST(l->info,clads_graph_edge_type*);
        na_info = CLADS_CAST(e->na->internal_info,radial_layout_node_information*);
        nb_info = CLADS_CAST(e->nb->internal_info,radial_layout_node_information*);
        ri = na_info->radius;
        rj = nb_info->radius;
        if (rc < ri + rj){
            rc = ri + rj;
        }
        l = l->next;
    }
    double theta = 0;
    clads_list_node_type *child;
    radial_layout_node_information *root_info = CLADS_CAST(root->internal_info,radial_layout_node_information*);
    clads_graph_node_type *child_node;
    while((child = clads_list_next(root_info->children))){
        child_node = CLADS_CAST(child->info,clads_graph_node_type*);
        theta += clads_graph_minimum_angular_space(child_node,rc);
    }
    double ra = (rc*theta)/(2*M_PI);
    return MAX(ra,rc);
}

static clads_void_type set_cartesian_coordinates(radial_layout_node_information* n,double base_radius,double center_x,double center_y){
    n->coordinates.x = (n->depth*base_radius)* cos(n->angle)+center_x;
    n->coordinates.y = (n->depth*base_radius)* sin(n->angle)+center_y;
}


static clads_void_type set_angle(clads_graph_node_type *v,double angle_remaining){
    radial_layout_node_information *info_v = CLADS_CAST(v->internal_info,radial_layout_node_information*);
    if (info_v->depth != 0)
        info_v->angle =  (info_v->angular_space / 2) + angle_remaining;

    double angle_taken = angle_remaining;

    clads_list_node_type *child;
    clads_graph_node_type *child_node;
    radial_layout_node_information *child_information;
    while((child = clads_list_next(info_v->children))){
        child_node = CLADS_CAST(child->info,clads_graph_node_type*);
        child_information = CLADS_CAST(child_node->internal_info,radial_layout_node_information*);
        set_angle(child_node,angle_taken);
        angle_taken += child_information->angular_space;
    }
}


clads_void_type
clads_graph_radial_layout(clads_graph_type *g,int tree_depth,double* width,double* height,double* base_radius,double* computing_time){
    clads_graph_node_type *root = CLADS_CAST(g->l_node->head->info,clads_graph_node_type*);
    radial_layout_node_information* info_root = CLADS_CAST(root->internal_info,radial_layout_node_information*);
    clock_t start = clock();
    (*base_radius) = clads_graph_get_base_radius(g,root);
    clads_graph_minimum_angular_space(root,*base_radius);
    clads_graph_angular_space(root,2*M_PI);
    set_angle(root,0.0);
    clock_t end = clock();
    (*computing_time) = (end-start)/(float)CLOCKS_PER_SEC;

    (*width) = ((*base_radius)*tree_depth*2)+(2*info_root->radius);
    (*height) = ((*base_radius)*tree_depth*2)+(2*info_root->radius);
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;
    while (l != NULL)
    {   
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        set_cartesian_coordinates(CLADS_CAST(n->internal_info,radial_layout_node_information*),(*base_radius),(*width)/2,(*height)/2);
        l = l->next;
    }
    
}

void clads_graph_export_radial_layout(clads_graph_type *g,char* filename,double width,double height,double ring_radius,int number_of_rings,double node_radius){
    FILE *file = fopen(filename, "w");
    if(file != NULL)
    {   
        fprintf(file,"%f %f %f %d %f\n",width,height,ring_radius,number_of_rings,node_radius);
        clads_list_node_type *l = g->l_node->head;
        clads_graph_node_type *n;
        cartesian_coordinates* coordinates;
        while (l != NULL)
        {   
            n = CLADS_CAST(l->info,clads_graph_node_type*);
            coordinates = CLADS_CAST(n->internal_info,cartesian_coordinates*);
            fprintf(file,"%llu %Lf %Lf\n",n->id,coordinates->x,coordinates->y);
            l = l->next;
        }
        l = g->l_edge->head;
        clads_graph_edge_type *e;
        fprintf(file,"#\n");
        while (l != NULL)
        {   
            e = CLADS_CAST(l->info,clads_graph_edge_type*);
            fprintf(file,"%llu %llu\n",e->na->id,e->nb->id);
            l = l->next;
        }
        fclose(file);
    }
   else
   {
      perror(filename);
   }
}


clads_void_type
clads_graph_finalize_force_layout(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *n;
    force_layout_node_information *info;
    while ((l = clads_list_next(g->l_node)))
    {      
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        info = CLADS_CAST(n->internal_info,force_layout_node_information*);
        info->node_neighbors->do_free_info = clads_false;
        clads_list_finalize(info->node_neighbors);
        CLADS_FREE(info->node_neighbors);
        CLADS_FREE(n->internal_info);
        n->internal_info = NULL;
    }
}

static double apply_attraction_x(clads_graph_node_type* node1, clads_graph_node_type* node2,double attraction,double distanciaSpring){
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = -attraction * (distance - distanciaSpring);
        return cos(angle) * force;      
}
static double apply_attraction_y(clads_graph_node_type* node1, clads_graph_node_type* node2,double attraction,double distanciaSpring){
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = -attraction * (distance - distanciaSpring);
        return sin(angle) * force;
}
static double apply_repulsion_x(clads_graph_node_type* node1, clads_graph_node_type* node2, double repulsion){
        force_layout_node_information *info1 = CLADS_CAST(node1->internal_info,force_layout_node_information*); 
        force_layout_node_information *info2 = CLADS_CAST(node2->internal_info,force_layout_node_information*); 
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = 0;
        if (distance != 0){
            force = (repulsion * info1->charge * info2->charge) /  pow (distance, 2);
        }
        return cos(angle) * force;
}
static double apply_repulsion_y(clads_graph_node_type* node1, clads_graph_node_type* node2, double repulsion){
        force_layout_node_information *info1 = CLADS_CAST(node1->internal_info,force_layout_node_information*); 
        force_layout_node_information *info2 = CLADS_CAST(node2->internal_info,force_layout_node_information*); 
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = 0;
        if (distance != 0){
            force = (repulsion * info1->charge * info2->charge) /  pow (distance, 2);
        }
        return sin(angle) * force;
}

static double get_minimum_x(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    cartesian_coordinates* coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
    double minimum_x = coordinates->x;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
        if (coordinates->x < minimum_x){
            minimum_x = coordinates->x;
        }
    }
    return minimum_x;
}

static double get_minimum_y(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    cartesian_coordinates* coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
    double minimum_y = coordinates->y;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        cartesian_coordinates* coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
        if (coordinates->y < minimum_y){
            minimum_y = coordinates->y;
        }
    }
    return minimum_y;
}

static double get_maximum_x(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    cartesian_coordinates* coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
    double maximum_x = coordinates->x;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        cartesian_coordinates* coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
        if (coordinates->x > maximum_x){
            maximum_x = coordinates->x;
        }
    }
    return maximum_x;
}

static double get_maximum_y(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    cartesian_coordinates* coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
    double maximum_y = coordinates->y;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        cartesian_coordinates* coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
        if (coordinates->y > maximum_y){
            maximum_y = coordinates->y;
        }
    }
    return maximum_y;
}


double get_height(clads_graph_type *g){
    double minimum_y = get_minimum_y(g);
    double maximum_y = get_maximum_y(g);
    return abs(maximum_y-minimum_y);
}

double get_width(clads_graph_type *g){
    double minimum_x = get_minimum_x(g);
    double maximum_x = get_maximum_x(g);
    return abs(maximum_x-minimum_x);
}

double clads_graph_get_area(clads_graph_type *g){
    return get_width(g) * get_height(g);
}

double clads_graph_get_aspect_ratio(clads_graph_type *g){
    double width = get_width(g);
    double height = get_height(g);
    if (width > height)
        return width/height;
    return height/width;
}

void clads_graph_adjust_to_screen(clads_graph_type *g,double w,double h,double margin){
    double width = get_width(g);
    double height = get_height(g);
    double difference_width = width-w;
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;

    cartesian_coordinates* coordinates;

    double difference_height = (difference_width*height)/width;
    double new_height = height-difference_height;
    double new_width = w;   
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
        coordinates->x = ((coordinates->x*w)/width);  
        coordinates->y = ((coordinates->y*new_height)/height);      
    }
    if (new_height > h){
        double difference_height = new_height-h;
        double difference_width = (difference_height*w)/new_height;
        new_width = w - difference_width;
        while ((l = clads_list_next(g->l_node)))
        {   
            iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
            coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
            coordinates->x = ((coordinates->x*new_width)/w);  
            coordinates->y = ((coordinates->y*h)/new_height);      
        }
        new_height = h;
    }
    double difference_x = get_minimum_x(g) - 15;
    double difference_y = get_minimum_y(g) - 15;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        coordinates = CLADS_CAST(iter_node->internal_info,cartesian_coordinates*);
        coordinates->x = coordinates->x - difference_x + ((w-new_width)/2);
        coordinates->y = coordinates->y - difference_y + ((h-new_height)/2);  
    }
}

double clads_graph_max_edge(clads_graph_type *g){
    double max_distance = 0;
    clads_list_node_type *l;
    clads_graph_edge_type *e;
    clads_graph_node_type *na;
    clads_graph_node_type *nb;
    double distance = 0;
    while ((l = clads_list_next(g->l_edge)))
    {   
        e = CLADS_CAST(l->info,clads_graph_edge_type*);
        na = CLADS_CAST(e->na,clads_graph_node_type*);
        nb = CLADS_CAST(e->nb,clads_graph_node_type*);
        distance = calc_distance(na,nb);
        if (distance > max_distance){
            max_distance = distance;
        }
    }
    return max_distance;
}

double clads_graph_lenght_edges(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_edge_type *e;
    clads_graph_node_type *na;
    clads_graph_node_type *nb;
    double total = 0;
    while ((l = clads_list_next(g->l_edge)))
    {   
        e = CLADS_CAST(l->info,clads_graph_edge_type*);
        na = CLADS_CAST(e->na,clads_graph_node_type*);
        nb = CLADS_CAST(e->nb,clads_graph_node_type*);
        total+=calc_distance(na,nb);
    }
    return total;
}

float clads_graph_get_smallest_angle(clads_graph_type *g){
    clads_list_node_type *list_inter1 = g->l_edge->head;
    clads_list_node_type *list_inter2 = g->l_edge->head;
    clads_graph_edge_type *e;
    clads_graph_edge_type *e2;
    clads_graph_node_type *na;
    clads_graph_node_type *nb;
    clads_graph_node_type *na2;
    clads_graph_node_type *nb2;
    float min_angle = 180.0;
    force_layout_node_information *na_info;
    force_layout_node_information *nb_info;
    force_layout_node_information *na2_info;
    force_layout_node_information *nb2_info;
    while (list_inter1 != NULL)
    {   
        e = CLADS_CAST(list_inter1->info,clads_graph_edge_type*);
        na = CLADS_CAST(e->na,clads_graph_node_type*);
        nb = CLADS_CAST(e->nb,clads_graph_node_type*);
        list_inter2 = list_inter1->next;
        while (list_inter2 != NULL)
        {   
            e2 = CLADS_CAST(list_inter2->info,clads_graph_edge_type*);
            na2 = CLADS_CAST(e2->na,clads_graph_node_type*);
            nb2 = CLADS_CAST(e2->nb,clads_graph_node_type*);
            na_info = CLADS_CAST(na->internal_info,force_layout_node_information*);
            nb_info = CLADS_CAST(nb->internal_info,force_layout_node_information*);
            na2_info = CLADS_CAST(na2->internal_info,force_layout_node_information*);
            nb2_info = CLADS_CAST(nb2->internal_info,force_layout_node_information*);
            unsigned int same_node = 1;
            double x1;
            double y1;
            double x2;
            double y2;
            double x3;
            double y3;
            if(na->id == na2->id){
                x3 = nb_info->coordinates.x;
                y3 = nb_info->coordinates.y;
                x2 = nb2_info->coordinates.x;
                y2 = nb2_info->coordinates.y;
                x1 = na_info->coordinates.x;
                y1 = na_info->coordinates.y;
            }
            else if(na->id == nb2->id){
                x3 = nb_info->coordinates.x;
                y3 = nb_info->coordinates.y;
                x2 = na2_info->coordinates.x;
                y2 = na2_info->coordinates.y;
                x1 = na_info->coordinates.x;
                y1 = na_info->coordinates.y;
            }
            else if(nb->id == na2->id){
                x3 = na_info->coordinates.x;
                y3 = na_info->coordinates.y;
                x2 = nb2_info->coordinates.x;
                y2 = nb2_info->coordinates.y;
                x1 = nb_info->coordinates.x;
                y1 = nb_info->coordinates.y;
            }
            else if(nb->id == nb2->id){
                x3 = na_info->coordinates.x;
                y3 = na_info->coordinates.y;
                x2 = na2_info->coordinates.x;
                y2 = na2_info->coordinates.y;
                x1 = nb_info->coordinates.x;
                y1 = nb_info->coordinates.y;
            }
            else{
                same_node = 0;
            }
            if (same_node == 1){
                double dx21 = x2-x1;
                double dx31 = x3-x1;
                double dy21 = y2-y1;
                double dy31 = y3-y1;
                double m12 = sqrt( dx21*dx21 + dy21*dy21 );
                double m13 = sqrt( dx31*dx31 + dy31*dy31 );
                double theta = acos( (dx21*dx31 + dy21*dy31) / (m12 * m13) );
                double deg = theta * 180.0 / M_PI;
                if (deg < min_angle){
                    min_angle = deg;
                }
            }
            list_inter2 = list_inter2->next;
        }
        list_inter1 = list_inter1->next;
    }
    return min_angle;
}


double
clads_graph_apply_forces(clads_graph_type *g,double attraction,double repulsion,double distanciaSpring,double timestep,double damping){
    clads_list_node_type *l = g->l_node->head;
    clads_list_node_type *l2;
    clads_graph_node_type *node;
    clads_graph_node_type *node2;
    clads_graph_node_type *neighbor;
    force_layout_node_information *info_node;
    double kinetic_energy = 0.0;
    while ((l = clads_list_next(g->l_node)))
    {   
        node = CLADS_CAST(l->info,clads_graph_node_type*);
        info_node = CLADS_CAST(node->internal_info,force_layout_node_information*);
        double net_force_x = 0.0;
        double net_force_y = 0.0;
        l2 = g->l_node->head;
        while (l2 != NULL)
        { 
             node2 = CLADS_CAST(l2->info,clads_graph_node_type*);
             if(node->id != node2->id){
                double nx = net_force_x;
                double ny = net_force_y;
                double cx = apply_repulsion_x(node, node2, repulsion);
                double cy = apply_repulsion_y(node, node2, repulsion);
                net_force_x = nx + cx;
                net_force_y = ny + cy;
             }
             l2 = l2->next;
        }
        while ((l2 = clads_list_next(info_node->node_neighbors)))
        {
            neighbor = CLADS_CAST(l2->info,clads_graph_node_type*);
            double nx = net_force_x;
            double ny = net_force_y;
            double hx = apply_attraction_x(node,neighbor,attraction,distanciaSpring);
            double hy = apply_attraction_y(node,neighbor,attraction,distanciaSpring);
            net_force_x = nx + hx;
            net_force_y = ny + hy;
        }
        double velocity_x = (info_node->velocity_x + timestep * net_force_x) * damping;
        double velocity_y = (info_node->velocity_y + timestep * net_force_y) * damping;
        info_node->velocity_x = velocity_x;
        info_node->velocity_y = velocity_y;
        double x = info_node->coordinates.x + timestep * info_node->velocity_x;
        double y = info_node->coordinates.y + timestep * info_node->velocity_y;
        info_node->coordinates.x = x;
        info_node->coordinates.y = y;       
        kinetic_energy = kinetic_energy + info_node->mass * (hypot(info_node->velocity_x, info_node->velocity_y));
     }
    return kinetic_energy;
}

void clads_graph_export_force_layout(clads_graph_type *g,char* filename){
    FILE *file = fopen(filename, "w");
    if(file != NULL)
    {    
        clads_list_node_type *l = g->l_node->head;
        clads_graph_node_type *n;
        cartesian_coordinates* coordinates;
        while (l != NULL)
        {   
            n = CLADS_CAST(l->info,clads_graph_node_type*);
            coordinates = CLADS_CAST(n->internal_info,cartesian_coordinates*);
            fprintf(file,"%llu %Lf %Lf\n",n->id,coordinates->x,coordinates->y);
            l = l->next;
        }
        l = g->l_edge->head;
        clads_graph_edge_type *e;
        fprintf(file,"#\n");
        while (l != NULL)
        {   
            e = CLADS_CAST(l->info,clads_graph_edge_type*);
            fprintf(file,"%llu %llu\n",e->na->id,e->nb->id);
            l = l->next;
        }
        fclose(file);
    }
   else
   {
      perror(filename);
   }
}

void clads_graph_force_layout(force_layout_run_info *f,int n,double e,double* computing_time){
    int x;
    double previous_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
    double kinetic_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
    clock_t start;
    clock_t end;
    do{
        previous_energy = kinetic_energy;
        for(x=0;x<n;x++){
            start = clock();
            kinetic_energy  = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
            end = clock();
            *computing_time += (end-start)/(float)CLOCKS_PER_SEC;
        }
        printf("previous: %f, now: %f\n",previous_energy,kinetic_energy);
    }while(kinetic_energy<(previous_energy-(previous_energy*e)));
}

void clads_graph_force_layout_with_statistics(force_layout_run_info *f,int n,double e){
    FILE *file = fopen("data/statistics.txt", "w");
    if(file != NULL)
    {    
        int x;
        unsigned int steps = 0;
        double previous_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
        double kinetic_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
        clock_t start;
        clock_t end;
        double elapsed_time = 0.0;
        fprintf(file,"# %i\n",n);
        do{
            previous_energy = kinetic_energy;
            for(x=0;x<n;x++){
                start = clock();
                kinetic_energy  = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
                end = clock();
                elapsed_time += (end-start)/(float)CLOCKS_PER_SEC;
                fprintf(file,"%u %f %f %f %f %f\n",steps,kinetic_energy,elapsed_time,
                    clads_graph_get_aspect_ratio(f->graph),
                    clads_graph_get_area(f->graph),
                    clads_graph_lenght_edges(f->graph));
                steps++;

            }
            printf("previous: %f, now: %f\n",previous_energy,kinetic_energy);
        }while(kinetic_energy<(previous_energy-(previous_energy*e)));
        fclose(file);
    }
   else
   {
      perror("data/statistics.txt");
   }
}
