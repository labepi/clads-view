# Copyright (C) 2012 Joao Paulo de Souza Medeiros
#
# Author(s): Joao Paulo de Souza Medeiros <ignotus21@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

"""
"""

import list
import clads_list
import clads_graph


class Node(object):
    """
    """
    def __init__(self, cobj):
        """
        """
        self.__cobj = cobj

    def get_cobj(self):
        """
        """
        return self.__cobj

    def get_info(self):
        """
        """
        return clads_graph.get_node_info(self.__cobj)

    def get_id(self):
        """
        """
        return clads_graph.get_node_id(self.__cobj)

    def get_clustering(self):
        """
        """
        return clads_graph.get_node_clustering(self.__cobj)

    def get_degree(self):
        """
        """
        return clads_graph.get_node_degree(self.__cobj)


class Edge(object):
    """
    """
    def __init__(self, cobj):
        """
        """
        self.__cobj = cobj

    def get_cobj(self):
        """
        """
        return self.__cobj

    def get_info(self):
        """
        """
        return clads_graph.get_edge_info(self.__cobj)

    def get_id(self):
        """
        """
        return clads_graph.get_edge_id(self.__cobj)

    def get_nodes(self):
        """
        """
        return clads_graph.get_edge_nodes(self.__cobj)


class Graph(object):
    """
    """
    def __init__(self):
        """
        """
        self.__cobj = clads_graph.initialize()

    def __len__(self):
        """
        """
        nodes, edges = self.size()
        return nodes

    def size(self):
        """
        """
        return clads_graph.size(self.__cobj)

    def get_cobj(self):
        """
        """
        return self.__cobj

    def add_node(self, info):
        """
        """
        return clads_graph.add_node(self.__cobj, info)

    def add_node_with_id(self, info, id):
        """
        """
        return clads_graph.add_node_with_id(self.__cobj, info, id)

    def add_edge(self, na, nb, info):
        """
        """
        return clads_graph.add_edge(self.__cobj, na, nb, info)

    def get_node(self,id):
        """
        """
        return clads_graph.get_node(self.__cobj, id)

def read_TGF(filename):
    g = Graph()
    input = open(filename, 'r').read().split('\n')
    adding_nodes = True
    for line in input:
        if line == '#' or line == '':
            adding_nodes = False
            continue
        if adding_nodes:
            args = line.split(' ')
            g.add_node_with_id(None,int(args[0]))
        else:
            args = line.split(' ')
            na = g.get_node(int(args[0]))
            nb = g.get_node(int(args[1]))
            g.add_edge(na, nb,None)
    return g

if __name__ == "__main__":
    
    filename = "../../test/ipv6-2008-12-5374-9426.txt"
    g = read_TGF(filename)
    na = Node(g.get_node(5373))
    print na, na.get_info(),na.get_id()