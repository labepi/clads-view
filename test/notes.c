void clads_graph_adjust_to_screen(clads_graph_type *g,double w,double h){
    double width = force_layout_get_width(g);
    double height = force_layout_get_height(g);
    double difference_width = width-w;
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    force_layout_node_information *iter_info;
    double difference_height = (difference_width*height)/width;
    double new_height = height-difference_height;

    printf("aspect_ratio: %f\n",clads_graph_get_force_layout_aspect_ratio(g));
    printf("width: %f,new_width: %f,differece_width: %f,height: %f,new_height: %f,difference_height: %f\n",width,w,difference_width,height,new_height,difference_height);
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        iter_info->x = ((iter_info->x*w)/width);  
        iter_info->y = ((iter_info->y*new_height)/height);      
    }
    printf("width: %f,height: %f,aspect_ratio: %f\n",force_layout_get_width(g),force_layout_get_height(g),clads_graph_get_force_layout_aspect_ratio(g));
    height = force_layout_get_height(g);
    if (height > h){
        clads_list_node_type *l;
        clads_graph_node_type *iter_node;
        force_layout_node_information *iter_info;
        double difference_height = height-h;
        double difference_width = (difference_height*w)/height;
        double new_width = w-difference_width;
        while ((l = clads_list_next(g->l_node)))
        {   
            iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
            iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
            iter_info->x = ((iter_info->x*new_width)/w);  
            iter_info->y = ((iter_info->y*h)/height);      
        }
    }
}


void clads_graph_adjust_to_screen(clads_graph_type *g,double w,double h){
    double width = force_layout_get_width(g);
    double height = force_layout_get_height(g);
    double difference_width = width-w;
    if (difference_width!=0){
        clads_list_node_type *l;
        clads_graph_node_type *iter_node;
        force_layout_node_information *iter_info;
        double difference_height = (difference_width*height)/width;
        double new_height = height-difference_height;

        printf("aspect_ratio: %f\n",clads_graph_get_force_layout_aspect_ratio(g));
        printf("width: %f,new_width: %f,differece_width: %f,height: %f,new_height: %f,difference_height: %f\n",width,w,difference_width,height,new_height,difference_height);
        while ((l = clads_list_next(g->l_node)))
        {   
            iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
            iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
            iter_info->x = ((iter_info->x*w)/width);  
            iter_info->y = ((iter_info->y*new_height)/height);      
        }
        printf("width: %f,height: %f,aspect_ratio: %f\n",force_layout_get_width(g),force_layout_get_height(g),clads_graph_get_force_layout_aspect_ratio(g));
    }
     width = force_layout_get_width(g);
    height = force_layout_get_height(g);
    if (height > h){
        clads_list_node_type *l;
        clads_graph_node_type *iter_node;
        force_layout_node_information *iter_info;
        double difference_height = height-h;
        double difference_width = (difference_height*width)/height;
        double new_width = width-difference_width;
        while ((l = clads_list_next(g->l_node)))
        {   
            iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
            iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
            iter_info->x = ((iter_info->x*new_width)/width);  
            iter_info->y = ((iter_info->y*h)/height);      
        }
    }
}

void centralize(clads_graph_type *g,double w,double h){
    double difference_x = force_layout_get_minimum_x(g) - 400;
    double difference_y = force_layout_get_minimum_y(g) - 190;
    double width = force_layout_get_width(g);
    double height = force_layout_get_height(g);

    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    force_layout_node_information *iter_info;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        iter_info->x = iter_info->x - difference_x + ((w-width)/2);
        iter_info->y = iter_info->y - difference_y + ((h-height)/2);  
    }

}



/**
 * Copyright (C) 2011 Joao Paulo de Souza Medeiros
 *
 * Author(s): Joao Paulo de Souza Medeiros <ignotus21@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#include "../code/clads.h"
#include "../code/graph.h"
#include <gtk/gtk.h>
typedef struct _components {
    GtkWidget *window;
    GtkWidget *graphNavigationPanel;
    GtkWidget *mainPanel;
    GtkWidget *drawingArea;
} components;

static     components this;

typedef struct retangulo {
    gdouble x;
    gdouble y;
    int direction_x;
    int direction_y;
    gdouble width;
    gdouble height;
} Retangulo;


force_layout_run_info f;

static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    return FALSE;
}

static void destroy( GtkWidget *widget,
                     gpointer   data )
{
    gtk_main_quit ();
}


static void createWindow() {
    this.window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width (GTK_CONTAINER (this.window), 10);
    gtk_window_set_title (GTK_WINDOW (this.window), "Graph Window");

    g_signal_connect (G_OBJECT (this.window), "delete_event",
              G_CALLBACK (delete_event), NULL);

    g_signal_connect (G_OBJECT (this.window), "destroy",
                G_CALLBACK (destroy), NULL);

}

static void layoutWidgets() {
    this.graphNavigationPanel = gtk_hbox_new (FALSE, 0);
    this.mainPanel = gtk_vbox_new(FALSE, 0);
    gtk_container_add  (GTK_CONTAINER (this.window), this.mainPanel);
    gtk_box_pack_start (GTK_BOX(this.mainPanel), this.graphNavigationPanel, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(this.mainPanel), this.drawingArea, TRUE, TRUE, 0);
}

static void show() {
    gtk_widget_show (this.drawingArea);
    gtk_widget_show (this.mainPanel);
    gtk_widget_show (this.graphNavigationPanel);
    gtk_widget_show (this.window);
}

static gboolean
expose_event( GtkWidget *widget, GdkEventExpose *event )
{
    printf("expose\n");
    cairo_t *cr = gdk_cairo_create(widget->window);
    cairo_set_source_rgb(cr, 255, 255, 0);
    cairo_rectangle(cr, 0, 0, widget->allocation.width,widget->allocation.height);
    cairo_fill(cr); 
  return FALSE;
}

static gboolean
configure_event( GtkWidget *widget, GdkEventConfigure *event )
{
    printf("configure\n");
    cairo_t *cr = gdk_cairo_create(widget->window);
    cairo_set_source_rgb(cr, 255, 0, 0);
    cairo_rectangle(cr, 0, 0, widget->allocation.width,widget->allocation.height);
    return TRUE;
}

static gboolean
button_press_event( GtkWidget *widget, GdkEventButton *event )
{
  if (event->button == 1){
    printf("1\n");
    clads_graph_force_layout(&f,150,0.09);
    }
  else{
    printf("2\n");
    clads_graph_adjust_to_screen(f.graph,f.expected_widght,f.expected_height,50);
    clads_graph_export_force_layout(f.graph,"test.txt");
  }
  return TRUE;
}

static void createDrawingArea() {
    this.drawingArea = gtk_drawing_area_new();

    gtk_signal_connect (GTK_OBJECT (this.drawingArea), "expose_event",
                  (GtkSignalFunc) expose_event, NULL);
    gtk_signal_connect (GTK_OBJECT(this.drawingArea),"configure_event",
                  (GtkSignalFunc) configure_event, NULL);
    gtk_signal_connect (GTK_OBJECT (this.drawingArea), "button_press_event",
                  (GtkSignalFunc) button_press_event, NULL);

    gtk_widget_set_events (this.drawingArea, GDK_EXPOSURE_MASK
                 | GDK_LEAVE_NOTIFY_MASK
                 | GDK_BUTTON_PRESS_MASK
                 | GDK_POINTER_MOTION_MASK
                 | GDK_POINTER_MOTION_HINT_MASK);

   

    gtk_drawing_area_size(CLADS_CAST(this.drawingArea,GtkDrawingArea*), 1100, 680);
}

static gboolean
draw_graph (GtkWidget *widget,force_layout_run_info *f)
{   
    clads_graph_type *g = f->graph;
    cairo_t *cr = gdk_cairo_create(widget->window);
    cairo_set_source_rgb(cr, 255, 255, 255);
    cairo_rectangle(cr, 0, 0, widget->allocation.width,widget->allocation.height);
    cairo_fill(cr); 

    double dashes[] = {5.0,5.0,5.0, 5.0};
    double offset = -50.0;
    //cairo_set_dash (cr, dashes, 1, offset);
    cairo_set_source_rgb(cr,0,255,255);
    cairo_rectangle(cr,force_layout_get_minimum_x(g),force_layout_get_minimum_y(g),force_layout_get_width(g),force_layout_get_height(g));
    cairo_fill(cr); 

    cairo_set_dash (cr, dashes, 1, offset);
    cairo_set_source_rgb(cr,255,0,0);

    cairo_rectangle(cr,15,15,f->expected_widght,f->expected_height);
    cairo_stroke(cr); 
    cairo_set_dash (cr, dashes, 0, offset);

    int text_x = 0;
    int text_y = 0;
    char to_string[50] = "";
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_set_font_size(cr,13);

    sprintf(to_string,"kinetic_energy: %f",f->kinetic_energy);
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"steps: %d",f->steps);
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"elapsed_time: %f",f->elapsed_time);
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"maximum x: %f",force_layout_get_maximum_x(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"minimum x: %f",force_layout_get_minimum_x(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"maximum y: %f",force_layout_get_maximum_y(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"minimum y: %f",force_layout_get_minimum_y(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"width: %f", force_layout_get_width(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"height: %f",force_layout_get_height(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"area: %f",clads_graph_get_force_layout_area(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"aspect ratio: %f",clads_graph_get_force_layout_aspect_ratio(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);

    sprintf(to_string,"total edge lenght: %f",clads_graph_force_layout_lenght_edges(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);
    
    sprintf(to_string,"smallest angle: %f",clads_graph_get_smallest_angle(g));
    cairo_move_to(cr, text_x, text_y+=13);
    cairo_show_text(cr,to_string);
    
    clads_list_node_type* l = g->l_edge->head;
    clads_graph_edge_type *e;
    force_layout_node_information *a1;
    force_layout_node_information *a2;
    cairo_set_line_width(cr,1);
    while (l != NULL)
    {   
        e = CLADS_CAST(l->info,clads_graph_edge_type*);
        a1 = CLADS_CAST(e->na->internal_info,force_layout_node_information*);
        a2 = CLADS_CAST(e->nb->internal_info,force_layout_node_information*);
        cairo_move_to(cr, a1->x, a1->y);
        cairo_line_to(cr, a2->x, a2->y);
        cairo_stroke(cr);
        l = l->next;
    }

    l = g->l_node->head;
    clads_graph_node_type *n;
    force_layout_node_information *a;
    cairo_set_font_size(cr,8);
    while (l != NULL)
    {   
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        a = CLADS_CAST(n->internal_info,force_layout_node_information*);
        sprintf(to_string,"%llu",n->id);
        cairo_move_to(cr, (a->x-3), (a->y-6));
        cairo_show_text(cr,to_string);
        cairo_arc(cr,a->x,a->y, 5, 0, 2*M_PI);
        cairo_fill(cr); 
        l = l->next;
    }

    return TRUE;
}


void
graph_print(clads_graph_type *g)
{
    printf("nodes\n");
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;
    while (l != NULL)
    {   
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        printf("address %p, id %llu, degree %lld\n",n,n->id,n->degree);
        l = l->next;
    }
    printf("edges\n");
    l = g->l_edge->head;
    clads_graph_edge_type *e;
    while (l != NULL)
    {   
        e = CLADS_CAST(l->info,clads_graph_edge_type*);
        printf("%llu --> %llu\n",e->na->id,e->nb->id);
        l = l->next;
    }
}

void
graph_force_print(clads_graph_type *g)
{
    printf("nodes\n");
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;
    force_layout_node_information *a;
    clads_list_node_type *l2;
    clads_graph_node_type *n2;
    while (l != NULL)
    {   
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        a = CLADS_CAST(n->internal_info,force_layout_node_information*);
        printf("id %llu, degree %lld, x %Lf, y %Lf \n",n->id,n->degree,a->x,a->y);
        while ((l2 = clads_list_next(a->node_neighbors)))
        {
            n2 = CLADS_CAST(l2->info,clads_graph_node_type*);
            printf("    id %llu, degree %lld\n",n2->id,n2->degree);
        }
        l = l->next;
    }
}

static gboolean
step(force_layout_run_info *f)
{
    clock_t start = clock();
    f->kinetic_energy = 0;//clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
    //refresh_position(f->graph);
    clock_t end = clock();
    f->elapsed_time+= (end-start)/(float)CLOCKS_PER_SEC;
    draw_graph(this.drawingArea,f);  
    return f->continuar;
}

void start_force_layout(int refresh_rate,force_layout_run_info *f){
    gtk_init (NULL,NULL);
    g_timeout_add(refresh_rate,CLADS_CAST(step,GSourceFunc),f);
    createWindow();
    createDrawingArea();
    layoutWidgets();
    show();
    gtk_main ();
}
int main( int   argc,
          char *argv[] )
{
    clads_graph_type g;
    clads_initialize();
    clads_graph_initialize_from_tgf_file(&g,"nodes.txt");

    clads_real_type max_x = 750;
    clads_real_type max_y = 600;
    clads_graph_initialize_force_layout(&g,max_x,max_y);
    //set_position(&g);
    int refresh_rate = 75;

    f.kinetic_energy = 0.0;
    f.steps = 0;
    f.elapsed_time = 0.0;
    f.attraction = 2.0;
    f.repulsion = 80000.0;
    f.distanciaSpring = 15;
    f.timestep  =  0.3;
    f.damping  =  0.15;
    f.graph = &g;
    f.continuar = TRUE;
    f.expected_widght = 750;
    f.expected_height = 600;
    //graph_force_print(&g);
    printf("%f\n",force_layout_get_maximum_x(&g));
    printf("%f\n",force_layout_get_minimum_x(&g));
    printf("%f\n",force_layout_get_maximum_y(&g));
    printf("%f\n",force_layout_get_minimum_y(&g));
    printf("widht %f\n",force_layout_get_width(&g));
    printf("height %f\n",force_layout_get_height(&g));
    start_force_layout(refresh_rate,&f);
    //i = 50;
    //e = 50;
    //double energy = clads_graph_apply_forces(f.graph,f.attraction,f.repulsion,f.distanciaSpring,f.timestep,f.damping);

    //clads_graph_finalize_force_layout(&g);
    clads_graph_finalize(&g);
    clads_finalize();
    return 0;
}

CC = cc
CFLAGS = -ggdb -Wall
LIBS = `pkg-config --cflags --libs gtk+-2.0`
default: all

all:
    cd code; ${CC} ${CFLAGS} -shared -fPIC -c *.c
    cd test; ${CC} ${CFLAGS} list.c -o list \
        ../code/clads.o \
        ../code/list.o -lm
    cd test; ${CC} ${CFLAGS} tree.c -o tree \
        ../code/clads.o \
        ../code/tree.o -lm
    cd test; ${CC} ${CFLAGS} graph.c -o graph ${LIBS}  \
        ../code/clads.o \
        ../code/list.o \
        ../code/statistic.o \
        ../code/graph.o -lm
    cd test; ${CC} ${CFLAGS} hash.c -o hash \
        ../code/clads.o \
        ../code/list.o \
        ../code/hash.o -lm
    cd bind; python setup.py build_ext -f -b clads

clean:
    cd code; rm -rf *.o
    cd test; rm -rf \
        list \
        tree \
        hash \
        graph
    cd bind; rm -rf clads/*.so clads/*.pyc \
        rm -rf build/




/**
 * Copyright (C) 2010-2012 Joao Paulo de Souza Medeiros
 *
 * Author(s): Joao Paulo de Souza Medeiros <ignotus21@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "graph.h"
#include <string.h>
#include <time.h> 


clads_order_type
clads_graph_default_f_compare(clads_graph_type *g,
                              clads_addr_type a,
                              clads_addr_type b)
{
    /*
     * As default threat as integer values.
     */
    if (*((int *) a) == *((int *) b))
        return clads_equal;
    if (*((int *) a) < *((int *) b))
        return clads_less;
    return clads_more;
}

clads_addr_type
clads_graph_edge_f_copy(clads_addr_type a)
{
    clads_graph_edge_type *f = CLADS_ALLOC(1, clads_graph_edge_type);
    clads_graph_edge_type *e = CLADS_CAST(a, clads_graph_edge_type *);

    f->na = e->na;
    f->nb = e->nb;
    f->info = e->info;
    f->key = e->key;

    return CLADS_CAST(f, clads_addr_type);
}

clads_addr_type
clads_graph_node_f_copy(clads_addr_type a)
{
    clads_graph_node_type *m = CLADS_ALLOC(1, clads_graph_node_type);
    clads_graph_node_type *n = CLADS_CAST(a, clads_graph_node_type *);

    m->id = n->id;
    m->clustering = n->clustering;
    m->info = n->info;
    m->key = n->key;

    return CLADS_CAST(m, clads_addr_type);
}

clads_void_type
clads_graph_initialize(clads_graph_type *g)
{
    g->n_node = 0;
    g->n_edge = 0;
    g->l_node = CLADS_ALLOC(1, clads_list_type);
    g->l_edge = CLADS_ALLOC(1, clads_list_type);
    g->allow_loop = clads_false;
    g->allow_multiple_edges = clads_false;
    g->is_directed = clads_false;
    g->l_adjacency = NULL;
    g->f_compare = &clads_graph_default_f_compare;
    g->more = NULL;

    clads_list_initialize(g->l_node);
    g->l_node->f_copy = &clads_graph_node_f_copy;
    g->l_node->do_free_info = clads_false;
    clads_list_initialize(g->l_edge);
    g->l_edge->f_copy = &clads_graph_edge_f_copy;
    g->l_edge->do_free_info = clads_false;
}

clads_graph_type *
clads_graph_copy(clads_graph_type *g)
{
    clads_graph_type *ng = CLADS_ALLOC(1, clads_graph_type);

    ng->n_node = g->n_node;
    ng->n_edge = g->n_edge;
    ng->allow_loop = g->allow_loop;
    ng->allow_multiple_edges = g->allow_multiple_edges;
    ng->is_directed = g->is_directed;
    ng->f_compare = g->f_compare;
    ng->more = g->more;

    ng->l_edge = clads_list_copy(g->l_edge);
    ng->l_node = clads_list_copy(g->l_node);

    if (g->l_adjacency != NULL)
        clads_graph_mount_adjacency(ng);
    else
        ng->l_adjacency = NULL;

    return ng;
}

clads_void_type
clads_graph_finalize(clads_graph_type *g)
{
    if (g != NULL)
    {
        clads_list_finalize(g->l_edge);
        clads_list_finalize(g->l_node);
        clads_graph_clear_adjacency(g);
    }
#if CLADS_DEBUG
    else
        printf("W. [GRAPH] Trying to finalize a NULL pointer.\n");
#endif
}

clads_graph_type *
clads_graph_new_erdos_nm(clads_size_type n,
                         clads_size_type m,
                         clads_bool_type is_directed,
                         clads_bool_type allow_loop,
                         clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_graph_node_type *na, *nb;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating nodes.
     */
    while (g->n_node < n)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating edges.
     */
    while (g->n_edge < m)
    {
        na = v[clads_randint(0, n - 1)];
        nb = v[clads_randint(0, n - 1)];

        clads_graph_add_edge(g, na, nb, NULL);
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_type *
clads_graph_new_erdos_np(clads_size_type n,
                         clads_real_type p,
                         clads_bool_type is_directed,
                         clads_bool_type allow_loop,
                         clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_size_type i, j, start;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating nodes.
     */
    while (g->n_node < n)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating edges.
     */
    for (i = 0; i < g->n_node; i++)
    {
        start = (g->is_directed == clads_true) ? 0 : i;

        for (j = start; j < g->n_node; j++)
        {
            if (clads_statistic_uniform_trial(p))
                clads_graph_add_edge(g, v[i], v[j], NULL);
        }
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_type *
clads_graph_new_watts(clads_size_type n,
                      clads_size_type k,
                      clads_real_type p,
                      clads_bool_type is_directed,
                      clads_bool_type allow_loop,
                      clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type *r, **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_graph_edge_type *e;
    clads_list_node_type *l;
    clads_size_type i, j;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating nodes.
     */
    while (g->n_node < n)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating edges (multiple edges and loops are threated by
     * `clads_graph_add_edge' function).
     */
    for (i = 0; i < g->n_node; i++)
    {
        clads_graph_add_edge(g, v[i], v[i], NULL);

        for (j = 1; j <= k / 2; j++)
        {
            clads_graph_add_edge(g, v[i], v[clads_loop_index(i + j, n)], NULL);
            clads_graph_add_edge(g, v[i], v[clads_loop_index(i - j, n)], NULL);
        }
    }

    /*
     * Rewiring edges.
     */
    while ((l = clads_list_next(g->l_edge)))
    {
        e = (clads_graph_edge_type *) l->info;

        if (clads_statistic_uniform_trial(p))
        {
            r = v[clads_randint(0, n - 1)];

            if (clads_statistic_uniform_trial(0.5))
                e->na = r;
            else
                e->nb = r;
        }
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_type *
clads_graph_new_barabasi(clads_size_type n,
                         clads_size_type m,
                         clads_bool_type is_directed,
                         clads_bool_type allow_loop,
                         clads_bool_type allow_multiple_edges)
{
    clads_graph_node_type **v = CLADS_ALLOC(n, clads_graph_node_type *);
    clads_graph_type *g = CLADS_ALLOC(1, clads_graph_type);
    clads_size_type count, index, drawn;

    clads_graph_initialize(g);

    g->is_directed = is_directed;
    g->allow_loop = allow_loop;
    g->allow_multiple_edges = allow_multiple_edges;

    /*
     * Creating the first `m' nodes.
     */
    while (g->n_node < m)
        v[g->n_node - 1] = clads_graph_add_node(g, NULL);

    /*
     * Creating the remaining `n' - `m' nodes.
     */
    while (g->n_node < n)
    {
        count = 0;
        index = g->n_node;

        v[index] = clads_graph_add_node(g, NULL);

        while (count < m)
        {
            // TODO: probabilistic function.
            drawn = 0;

            if (clads_graph_add_edge(g, v[index], v[drawn], NULL))
                count++;
        }
    }

    CLADS_FREE(v);

    return g;
}

clads_graph_edge_type *
clads_graph_edge_new(clads_void_type)
{
    clads_graph_edge_type *e = CLADS_ALLOC(1, clads_graph_edge_type);

    e->na = NULL;
    e->nb = NULL;
    e->info = NULL;
    e->key = clads_off;

    return e;
}

clads_void_type
clads_graph_clear_adjacency(clads_graph_type *g)
{
    clads_size_type i;

    if (g->l_adjacency != NULL)
    {
        for (i = 0; i < g->n_node; i++)
        {
            clads_list_finalize(g->l_adjacency[i]);
            CLADS_FREE(g->l_adjacency[i]);
        }

        CLADS_FREE(g->l_adjacency);
        g->l_adjacency = NULL;
    }
}

clads_void_type
clads_graph_mount_adjacency(clads_graph_type *g)
{
    clads_graph_edge_type *e;
    clads_list_node_type *n, *p;
    clads_size_type i;

    /*
     * Clear the adjacency list if it already exists, and create a new one.
     */
    clads_graph_clear_adjacency(g);

    g->l_adjacency = CLADS_ALLOC(g->n_node, clads_list_type *);

    /*
     * Initializing lists.
     */
    for (i = 0; i < g->n_node; i++)
    {
        g->l_adjacency[i] = CLADS_ALLOC(1, clads_list_type);
        clads_list_initialize(g->l_adjacency[i]);
    }

    /*
     * Fill the adjacency list.
     */
    while ((p = clads_list_next(g->l_edge)))
    {
        e = (clads_graph_edge_type *) p->info;

        n = clads_list_node_new();
        n->info = (clads_addr_type) e->nb;
        clads_list_insert(g->l_adjacency[e->na->id], n);

        if (!g->is_directed)
        {
            n = clads_list_node_new();
            n->info = (clads_addr_type) e->na;
            clads_list_insert(g->l_adjacency[e->nb->id], n);
        }
    }
}

clads_list_type *
clads_graph_get_edges_by_nodes(clads_graph_type *g,
                               clads_graph_node_type *na,
                               clads_graph_node_type *nb)
{
    clads_list_node_type *l = g->l_edge->head;
    clads_graph_edge_type *e;
    clads_list_node_type *p;

    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);
    clads_list_initialize(r);

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if ((e->na == na && e->nb == nb) ||
            (!g->is_directed && e->na == nb && e->nb == na))
        {
            p = clads_list_node_new();
            p->info = e;
            clads_list_insert(r, p);
        }

        l = l->next;
    }

    if (clads_list_is_empty(r) == clads_true)
    {
        clads_list_finalize(r);
        CLADS_FREE(r);
        r = NULL;
    }

    return r;
}

clads_graph_edge_type *
clads_graph_get_edge_by_nodes(clads_graph_type *g,
                              clads_graph_node_type *na,
                              clads_graph_node_type *nb)
{
    clads_list_node_type *l = g->l_edge->head;
    clads_graph_edge_type *e;

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if ((e->na == na && e->nb == nb) ||
            (!g->is_directed && e->na == nb && e->nb == na))
            return e;

        l = l->next;
    }

    return NULL;
}

clads_graph_node_type *
clads_graph_get_node(clads_graph_type *g,
                     clads_id_type id)
{
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;

    while (l != NULL)
    {
        n = (clads_graph_node_type *) l->info;
        if (n->id == id){
            return n;
        }
        l = l->next;
    }
    return NULL;
}

clads_graph_node_type *
clads_graph_get_node_by_info(clads_graph_type *g,
                             clads_addr_type info)
{
    clads_list_node_type *l = g->l_edge->head;
    clads_graph_node_type *n;

    while (l != NULL)
    {
        n = (clads_graph_node_type *) l->info;

        if (g->f_compare(g, n->info, info) == clads_equal)
            return n;

        l = l->next;
    }

    return NULL;
}

clads_graph_edge_type *
clads_graph_add_edge(clads_graph_type *g,
                     clads_graph_node_type *na,
                     clads_graph_node_type *nb,
                     clads_addr_type info)
{
    clads_graph_edge_type *e;
    clads_list_node_type *p;
    if (g->allow_loop == clads_false && na == nb)
        return NULL;

    if (g->allow_multiple_edges == clads_false &&
        clads_graph_get_edge_by_nodes(g, na, nb) != NULL)
        return NULL;

    e = CLADS_ALLOC(1, clads_graph_edge_type);

    e->id = g->n_edge++;

    e->na = na;
    e->nb = nb;
    e->info = info;

    p = clads_list_node_new();
    p->info = (clads_addr_type) e;
    clads_list_insert(g->l_edge, p);

    return e;
}

clads_graph_node_type *
clads_graph_add_node(clads_graph_type *g,
                     clads_addr_type info)
{
    clads_graph_node_type *n = CLADS_ALLOC(1, clads_graph_node_type);
    clads_list_node_type *p;

    n->id = g->n_node++;
    n->info = info;
    p = clads_list_node_new();
    p->info = (clads_addr_type) n;
    clads_list_insert(g->l_node, p);

    return n;
}

clads_graph_node_type *
clads_graph_add_node_with_id(clads_graph_type *g,
                     clads_addr_type info,clads_id_type id)
{   
    clads_graph_node_type *n;
    n = clads_graph_get_node(g,id);
    if (n != NULL ){
        printf("already exists a node with id: %llu\n",id);
        return NULL;
    }
    n = CLADS_ALLOC(1, clads_graph_node_type);
    clads_list_node_type *p;

    n->id = id;
    n->info = info;
    n->degree = 0;
    p = clads_list_node_new();
    p->info = (clads_addr_type) n;
    clads_list_insert(g->l_node, p);
    g->n_node++;
    return n;

}

clads_list_type *
clads_graph_get_edges_by_node(clads_graph_type *g,
                              clads_graph_node_type *n)
{
    clads_list_node_type *p, *l = g->l_edge->head;
    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);
    clads_graph_edge_type *e;

    clads_list_initialize(r);

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if (e->na == n || e->nb == n)
        {
            p = clads_list_node_new();
            p->info = (clads_addr_type) e;
            clads_list_insert(r, p);
        }

        l = l->next;
    }

    return r;
}

clads_list_type *
clads_graph_get_node_neighbors(clads_graph_type *g,
                               clads_graph_node_type *n)
{
    clads_list_node_type *p, *l = g->l_edge->head;
    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);
    clads_graph_edge_type *e;

    clads_list_initialize(r);

    while (l != NULL)
    {
        e = (clads_graph_edge_type *) l->info;

        if (e->na == n || e->nb == n)
        {
            p = clads_list_node_new();

            if (e->na == n)
                p->info = (clads_addr_type) e->nb;
            else
                p->info = (clads_addr_type) e->na;

            clads_list_insert(r, p);
        }

        l = l->next;
    }

    return r;
}

clads_real_type
clads_graph_clustering(clads_graph_type *g)
{
    clads_list_node_type *l = g->l_node->head;
    clads_real_type size = g->n_node;
    clads_graph_node_type *n;

    g->clustering = 0;

    while (l != NULL)
    {
        n = (clads_graph_node_type *) l->info;
        g->clustering += clads_graph_node_clustering(g, n);

        l = l->next;
    }

    g->clustering = g->clustering / size;

    return g->clustering;
}

clads_real_type
clads_graph_node_clustering(clads_graph_type *g,
                            clads_graph_node_type *n)
{
    // TODO

    return 0;
}

clads_list_type *
clads_graph_spanning_tree(clads_graph_type *g)
{
    clads_graph_node_type *a, *b;
    clads_graph_edge_type *e;
    clads_list_node_type *l;
    clads_list_type *q = CLADS_ALLOC(1, clads_list_type);
    clads_list_type *r = CLADS_ALLOC(1, clads_list_type);

    clads_list_initialize(q);
    clads_list_initialize(r);

    /*
     * Initialize nodes as unvisited.
     */
    while ((l = clads_list_next(g->l_node)))
    {
        a = CLADS_CAST(l->info, clads_graph_node_type *);
        a->key = clads_off;
    }

    /*
     * Using breadth-first-search to select the edges for the spanning tree.
     */
    clads_graph_mount_adjacency(g);

    a = CLADS_CAST(g->l_node->head->info, clads_graph_node_type *);
    a->key = clads_on;

    l = clads_list_node_new();
    l->info = a;

    clads_list_enqueue(q, l);

    while (clads_list_is_empty(q) == clads_false)
    {
        l = clads_list_dequeue(q);
        a = CLADS_CAST(l->info, clads_graph_node_type *);

        /*
         * Iterate over each neighbor of node `a'.
         */
        while ((l = clads_list_next(g->l_adjacency[a->id])))
        {
            b = CLADS_CAST(l, clads_graph_node_type *);

            /*
             * If node `b' not visited yet, do it.
             */
            if (b->key == clads_off)
            {
                b->key = clads_on;

                l = clads_list_node_new();
                l->info = b;
                clads_list_enqueue(q, l);

                e = clads_graph_edge_new();
                e->na = a;
                e->nb = b;
                l = clads_list_node_new();
                l->info = e;
                clads_list_insert(r, l);
            }
        }
    }

    if (clads_list_is_empty(r) == clads_true)
    {
        clads_list_finalize(r);
        CLADS_FREE(r);
        r = NULL;
    }

    return r;
}

clads_void_type
clads_graph_initialize_from_tgf_file(clads_graph_type *g,char *filename)
{  
    FILE *file = fopen(filename, "r");
    if(file != NULL)
    {       
        clads_graph_initialize(g);
        char line[100];
        while(strcmp(fgets(line, 100, file),"#\n") != 0){
            clads_graph_add_node_with_id(g,NULL,strtoull(line,NULL,10));
        }
        while(fgets(line, 100, file) != NULL){
            clads_graph_node_type* na =  clads_graph_get_node(g,strtoull (strtok (line," "),NULL,10));
            clads_graph_node_type* nb =  clads_graph_get_node(g,strtoull (strtok (NULL," "),NULL,10));
            clads_graph_add_edge(g,na,nb,NULL);
        }
        fclose(file);
    }
   else
   {
      perror(filename);
   }
}


clads_void_type
clads_graph_initialize_force_layout(clads_graph_type *g,clads_real_type max_x, clads_real_type max_y){
    
    clads_list_node_type *l;
    force_layout_node_information *info;
    clads_graph_node_type *n;
    while ((l = clads_list_next(g->l_node)))
    {
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        info = CLADS_ALLOC(1,force_layout_node_information);
        n->internal_info = info;
        info->x =  ((clads_real_type)rand() / (clads_real_type)(RAND_MAX / max_x));
        info->y =  ((clads_real_type)rand() / (clads_real_type)(RAND_MAX / max_y));
        info->velocity_x = 0;
        info->velocity_y = 0;
        info->charge = 1;
        info->mass = 1;
        info->net_force_x = 0;
        info->net_force_y = 0;
        info->node_neighbors = clads_graph_get_node_neighbors(g,n);
    }
   
}

clads_void_type
clads_graph_initialize_radial_layout(clads_graph_type *g, clads_real_type radius){
    clads_list_node_type *l;
    radial_layout_node_information *info_a;
    radial_layout_node_information *info_b;
    clads_graph_node_type *n;

    while ((l = clads_list_next(g->l_node)))
    {
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        info_a = CLADS_ALLOC(1,radial_layout_node_information);
        n->internal_info = info_a;
        info_a->x =  0;
        info_a->y =  0;
        info_a->radius = radius;
        info_a->angle = 0;
        info_a->angular_space = 0;
        info_a->min_angular_space = 0;
        info_a->parent = NULL;
        info_a->children = CLADS_ALLOC(1, clads_list_type);
        clads_list_initialize(info_a->children);
    }

    clads_graph_node_type *a, *b;

    clads_list_type *q = CLADS_ALLOC(1, clads_list_type);

    clads_list_initialize(q);

    while ((l = clads_list_next(g->l_node)))
    {
        a = CLADS_CAST(l->info, clads_graph_node_type *);
        info_a = CLADS_CAST(a->internal_info, radial_layout_node_information*);
        a->key = clads_off;
        info_a->depth = 0;
    }

    clads_graph_mount_adjacency(g);
    
    a = CLADS_CAST(g->l_node->head->info, clads_graph_node_type *);
    a->key = clads_on;

    l = clads_list_node_new();
    l->info = a;

    clads_list_enqueue(q, l);

    clads_list_node_type *node_child;
    while (clads_list_is_empty(q) == clads_false)
    {
        l = clads_list_dequeue(q);
        a = CLADS_CAST(l->info, clads_graph_node_type *);
        info_a = CLADS_CAST(a->internal_info, radial_layout_node_information*);
        while ((l = clads_list_next(g->l_adjacency[a->id])))
        {
            b = CLADS_CAST(l->info, clads_graph_node_type *);
            info_b = CLADS_CAST(b->internal_info, radial_layout_node_information*);
            if (b->key == clads_off)
            {
                b->key = clads_on;

                info_b->parent = a;
                node_child = clads_list_node_new();
                node_child->info = b;
                clads_list_insert(info_a->children,node_child);
                info_b->depth = info_a->depth+1;

                l = clads_list_node_new();
                l->info = b;
                clads_list_enqueue(q, l);
            }
        }
    }
}

clads_void_type
clads_graph_radial_layout(clads_graph_type *g){
    clads_graph_node_type *root = CLADS_CAST(g->l_node->head->info,clads_graph_node_type*);
    clads_graph_minimum_angular_space(root,75.0);
    clads_graph_angular_space(root,2*M_PI);
    set_angle(root,0.0);
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;
    while (l != NULL)
    {   
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        set_cartesian_coordinates(CLADS_CAST(n->internal_info,radial_layout_node_information*),75.0);
        l = l->next;
    }
}
clads_void_type set_cartesian_coordinates(radial_layout_node_information* n,double base_radius){
    n->x = (n->depth*base_radius)* cos(n->angle);
    n->y = (n->depth*base_radius)* sin(n->angle);
}
clads_void_type
clads_graph_minimum_angular_space(clads_graph_node_type *v,double base_radius){
    double j = 0;
    double s = 0;
    radial_layout_node_information *iv = CLADS_CAST(v->internal_info,radial_layout_node_information*);
    clads_list_node_type *child;
    clads_graph_node_type *child_node;
    radial_layout_node_information *child_information;
    if (iv->depth != 0)
        j = 2 * asin(iv->radius/(iv->depth*base_radius));
    while((child = clads_list_next(iv->children))){
        child_node = CLADS_CAST(child->info,clads_graph_node_type*);
        child_information = CLADS_CAST(child_node->internal_info,radial_layout_node_information*);
        clads_graph_minimum_angular_space(child_node,base_radius);
        s += child_information->min_angular_space;
    }
    iv->min_angular_space = MAX(j,s);
   
}

clads_void_type
clads_graph_angular_space(clads_graph_node_type *v,double remaining_angular_space){
    radial_layout_node_information *info_v = CLADS_CAST(v->internal_info,radial_layout_node_information*);
    if (info_v->depth == 0){
        info_v->angular_space = 2*M_PI;
    }
    else{
        radial_layout_node_information *info_parent = CLADS_CAST(info_v->parent->internal_info,radial_layout_node_information*);
        info_v->angular_space = remaining_angular_space*info_v->min_angular_space/info_parent->min_angular_space;
    }

    clads_list_node_type *child;
    clads_graph_node_type *child_node;
    while((child = clads_list_next(info_v->children))){
        child_node = CLADS_CAST(child->info,clads_graph_node_type*);
        clads_graph_angular_space(child_node,info_v->angular_space);
    }

}
clads_void_type set_angle(clads_graph_node_type *v,double angle_remaining){
    radial_layout_node_information *info_v = CLADS_CAST(v->internal_info,radial_layout_node_information*);
    if (info_v->depth == 0)
        info_v->angle =  (info_v->angular_space / 2) + angle_remaining;

    double angle_taken = angle_remaining;

    clads_list_node_type *child;
    clads_graph_node_type *child_node;
    radial_layout_node_information *child_information;
    while((child = clads_list_next(info_v->children))){
        child_node = CLADS_CAST(child->info,clads_graph_node_type*);
        child_information = CLADS_CAST(child_node->internal_info,radial_layout_node_information*);
        set_angle(child_node,angle_taken);
        angle_taken += child_information->angular_space;
    }
}

clads_void_type
clads_graph_finalize_force_layout(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *n;
    force_layout_node_information *info;
    while ((l = clads_list_next(g->l_node)))
    {      
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        info = CLADS_CAST(n->internal_info,force_layout_node_information*);
        clads_list_finalize(info->node_neighbors);
        CLADS_FREE(info);
        n->internal_info = NULL;
    }
}

  
static clads_real_type calc_delta_x(force_layout_node_information* node1, force_layout_node_information* node2){
        return (node1->x - node2->x);
}

static clads_real_type calc_delta_y(force_layout_node_information* node1, force_layout_node_information* node2){
        return (node1->y - node2->y);
}

static float calc_distance(clads_graph_node_type* node1, clads_graph_node_type* node2){
        force_layout_node_information *info1 = CLADS_CAST(node1->internal_info,force_layout_node_information*); 
        force_layout_node_information *info2 = CLADS_CAST(node2->internal_info,force_layout_node_information*); 
        clads_real_type delta_x = calc_delta_x(info1,info2);
        clads_real_type delta_y = calc_delta_y(info1,info2);
        return hypot(delta_x, delta_y);
}
static float calc_angle(clads_graph_node_type* node1, clads_graph_node_type* node2){
        force_layout_node_information *info1 = CLADS_CAST(node1->internal_info,force_layout_node_information*); 
        force_layout_node_information *info2 = CLADS_CAST(node2->internal_info,force_layout_node_information*); 
        clads_real_type delta_x = calc_delta_x(info1,info2);
        clads_real_type delta_y = calc_delta_y(info1,info2);
        return atan2(delta_y, delta_x);
}
static double apply_attraction_x(clads_graph_node_type* node1, clads_graph_node_type* node2,double attraction,double distanciaSpring){
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = -attraction * (distance - distanciaSpring);
        return cos(angle) * force;      
}
static double apply_attraction_y(clads_graph_node_type* node1, clads_graph_node_type* node2,double attraction,double distanciaSpring){
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = -attraction * (distance - distanciaSpring);
        return sin(angle) * force;
}
static double apply_repulsion_x(clads_graph_node_type* node1, clads_graph_node_type* node2, double repulsion){
        force_layout_node_information *info1 = CLADS_CAST(node1->internal_info,force_layout_node_information*); 
        force_layout_node_information *info2 = CLADS_CAST(node2->internal_info,force_layout_node_information*); 
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = 0;
        if (distance != 0){
            force = (repulsion * info1->charge * info2->charge) /  pow (distance, 2);
        }
        return cos(angle) * force;
}
static double apply_repulsion_y(clads_graph_node_type* node1, clads_graph_node_type* node2, double repulsion){
        force_layout_node_information *info1 = CLADS_CAST(node1->internal_info,force_layout_node_information*); 
        force_layout_node_information *info2 = CLADS_CAST(node2->internal_info,force_layout_node_information*); 
        float distance = calc_distance(node1, node2);
        float angle = calc_angle(node1, node2);
        double force = 0;
        if (distance != 0){
            force = (repulsion * info1->charge * info2->charge) /  pow (distance, 2);
        }
        return sin(angle) * force;
}

double force_layout_get_minimum_x(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    force_layout_node_information *iter_info;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
    double minimum_x = iter_info->x;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        if (iter_info->x < minimum_x){
            minimum_x = iter_info->x;
        }
    }
    return minimum_x;
}

double force_layout_get_minimum_y(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    force_layout_node_information *iter_info;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
    double minimum_y = iter_info->y;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        if (iter_info->y < minimum_y){
            minimum_y = iter_info->y;
        }
    }
    return minimum_y;
}

double force_layout_get_maximum_x(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    force_layout_node_information *iter_info;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
    double maximum_x = iter_info->x;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        if (iter_info->x > maximum_x){
            maximum_x = iter_info->x;
        }
    }
    return maximum_x;
}

double force_layout_get_maximum_y(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    force_layout_node_information *iter_info;
    l = clads_list_next(g->l_node);
    iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
    iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
    double maximum_y = iter_info->y;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        if (iter_info->y > maximum_y){
            maximum_y = iter_info->y;
        }
    }
    return maximum_y;
}


double force_layout_get_height(clads_graph_type *g){
    double minimum_y = force_layout_get_minimum_y(g);
    double maximum_y = force_layout_get_maximum_y(g);
    return abs(maximum_y-minimum_y);
}

double force_layout_get_width(clads_graph_type *g){
    double minimum_x = force_layout_get_minimum_x(g);
    double maximum_x = force_layout_get_maximum_x(g);
    return abs(maximum_x-minimum_x);
}

double clads_graph_get_force_layout_area(clads_graph_type *g){
    return force_layout_get_width(g) * force_layout_get_height(g);
}

double clads_graph_get_force_layout_aspect_ratio(clads_graph_type *g){
    double width = force_layout_get_width(g);
    double height = force_layout_get_height(g);
    if (width > height)
        return width/height;
    return height/width;
}

void clads_graph_adjust_to_screen(clads_graph_type *g,double w,double h,double margin){
    double width = force_layout_get_width(g);
    double height = force_layout_get_height(g);
    double difference_width = width-w;
    clads_list_node_type *l;
    clads_graph_node_type *iter_node;
    force_layout_node_information *iter_info;
    double difference_height = (difference_width*height)/width;
    double new_height = height-difference_height;
    double new_width = w;   
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        iter_info->x = ((iter_info->x*w)/width);  
        iter_info->y = ((iter_info->y*new_height)/height);      
    }
    if (new_height > h){
        double difference_height = new_height-h;
        double difference_width = (difference_height*w)/new_height;
        new_width = w - difference_width;
        while ((l = clads_list_next(g->l_node)))
        {   
            iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
            iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
            iter_info->x = ((iter_info->x*new_width)/w);  
            iter_info->y = ((iter_info->y*h)/new_height);      
        }
        new_height = h;
    }
    double difference_x = force_layout_get_minimum_x(g) - 15;
    double difference_y = force_layout_get_minimum_y(g) - 15;
    while ((l = clads_list_next(g->l_node)))
    {   
        iter_node = CLADS_CAST(l->info,clads_graph_node_type*);
        iter_info = CLADS_CAST(iter_node->internal_info,force_layout_node_information*);
        iter_info->x = iter_info->x - difference_x + ((w-new_width)/2);
        iter_info->y = iter_info->y - difference_y + ((h-new_height)/2);  
    }
}

double clads_graph_force_layout_lenght_edges(clads_graph_type *g){
    clads_list_node_type *l;
    clads_graph_edge_type *e;
    clads_graph_node_type *na;
    clads_graph_node_type *nb;
    double total = 0;
    while ((l = clads_list_next(g->l_edge)))
    {   
        e = CLADS_CAST(l->info,clads_graph_edge_type*);
        na = CLADS_CAST(e->na,clads_graph_node_type*);
        nb = CLADS_CAST(e->nb,clads_graph_node_type*);
        total+=calc_distance(na,nb);
    }
    return total;
}

float clads_graph_get_smallest_angle(clads_graph_type *g){
    //printf("=============clads_graph_get_smallest_angle==============\n");
    clads_list_node_type *list_inter1 = g->l_edge->head;
    clads_list_node_type *list_inter2 = g->l_edge->head;
    clads_graph_edge_type *e;
    clads_graph_edge_type *e2;
    clads_graph_node_type *na;
    clads_graph_node_type *nb;
    clads_graph_node_type *na2;
    clads_graph_node_type *nb2;
    float  total = 10.0;
    force_layout_node_information *na_info;
    force_layout_node_information *nb_info;
    force_layout_node_information *na2_info;
    force_layout_node_information *nb2_info;
    while (list_inter1 != NULL)
    {   
        e = CLADS_CAST(list_inter1->info,clads_graph_edge_type*);
        na = CLADS_CAST(e->na,clads_graph_node_type*);
        nb = CLADS_CAST(e->nb,clads_graph_node_type*);
        list_inter2 = list_inter1->next;
        while (list_inter2 != NULL)
        {   
            e2 = CLADS_CAST(list_inter2->info,clads_graph_edge_type*);
            na2 = CLADS_CAST(e2->na,clads_graph_node_type*);
            nb2 = CLADS_CAST(e2->nb,clads_graph_node_type*);
            na_info = CLADS_CAST(na->internal_info,force_layout_node_information*);
            nb_info = CLADS_CAST(nb->internal_info,force_layout_node_information*);
            na2_info = CLADS_CAST(na2->internal_info,force_layout_node_information*);
            nb2_info = CLADS_CAST(nb2->internal_info,force_layout_node_information*);
            unsigned int same_node = 1;
            double x1;
            double y1;
            double x2;
            double y2;
            double x3;
            double y3;
            if(na->id == na2->id){
                x3 = nb_info->x;
                y3 = nb_info->y;
                x2 = nb2_info->x;
                y2 = nb2_info->y;
                x1 = na_info->x;
                y1 = na_info->y;
            }
            else if(na->id == nb2->id){
                x3 = nb_info->x;
                y3 = nb_info->y;
                x2 = na2_info->x;
                y2 = na2_info->y;
                x1 = na_info->x;
                y1 = na_info->y;
            }
            else if(nb->id == na2->id){
                x3 = na_info->x;
                y3 = na_info->y;
                x2 = nb2_info->x;
                y2 = nb2_info->y;
                x1 = nb_info->x;
                y1 = nb_info->y;
            }
            else if(nb->id == nb2->id){
                x3 = na_info->x;
                y3 = na_info->y;
                x2 = na2_info->x;
                y2 = na2_info->y;
                x1 = nb_info->x;
                y1 = nb_info->y;
            }
            else{
                same_node = 0;
            }
            if (same_node == 1){
                double dx21 = x2-x1;
                double dx31 = x3-x1;
                double dy21 = y2-y1;
                double dy31 = y3-y1;
                double m12 = sqrt( dx21*dx21 + dy21*dy21 );
                double m13 = sqrt( dx31*dx31 + dy31*dy31 );
                double theta = acos( (dx21*dx31 + dy21*dy31) / (m12 * m13) );
                double deg = theta * 180.0 / M_PI;
                //double final_deg;
                if (deg > 90){
                    //final_deg = 180 - deg;
                } 
                else{
                    //final_deg  = deg;
                }
                //double angle1 = atan2(na_info->y - nb_info->y , na_info->x - nb_info->x);
                //double angle2 = atan2(na2_info->y - nb2_info->y , na2_info->x - nb2_info->x);
                //double angle = (angle1-angle2) * (180.0 / M_PI);
                double m1 = (na_info->y - nb_info->y) / (na_info->x - nb_info->x);
                double m2 = (na2_info->y - nb2_info->y) / (na2_info->x - nb2_info->x);
                double theta1 = atan(m1) * (180.0 / M_PI);
                double theta2 = atan(m2) * (180.0 / M_PI);
                double angletheta = abs(theta2 - theta1);
                //double final_theta;
                if (angletheta > 90){
                    //final_theta = 180 - angletheta;
                } 
                else{
                    //final_theta  = angletheta;
                }
                //printf("%llu --> %llu - %llu --> %llu: %f/ %f,%f,%f\n",na->id,nb->id,na2->id,nb2->id,deg,final_deg,angletheta,final_theta);
            }
            list_inter2 = list_inter2->next;
        }
        list_inter1 = list_inter1->next;
    }
    return total;
}


double
clads_graph_apply_forces(clads_graph_type *g,double attraction,double repulsion,double distanciaSpring,double timestep,double damping){
    clads_list_node_type *l = g->l_node->head;
    clads_list_node_type *l2;
    clads_graph_node_type *node;
    clads_graph_node_type *node2;
    clads_graph_node_type *neighbor;
    force_layout_node_information *info_node;
    double kinetic_energy = 0.0;
    while ((l = clads_list_next(g->l_node)))
    {   
        node = CLADS_CAST(l->info,clads_graph_node_type*);
        info_node = CLADS_CAST(node->internal_info,force_layout_node_information*);
        double net_force_x = 0.0;
        double net_force_y = 0.0;
        l2 = g->l_node->head;
        while (l2 != NULL)
        { 
             node2 = CLADS_CAST(l2->info,clads_graph_node_type*);
             if(node->id != node2->id){
                double nx = net_force_x;
                double ny = net_force_y;
                double cx = apply_repulsion_x(node, node2, repulsion);
                double cy = apply_repulsion_y(node, node2, repulsion);
                net_force_x = nx + cx;
                net_force_y = ny + cy;
             }
             l2 = l2->next;
        }
        while ((l2 = clads_list_next(info_node->node_neighbors)))
        {
            neighbor = CLADS_CAST(l2->info,clads_graph_node_type*);
            double nx = net_force_x;
            double ny = net_force_y;
            double hx = apply_attraction_x(node,neighbor,attraction,distanciaSpring);
            double hy = apply_attraction_y(node,neighbor,attraction,distanciaSpring);
            net_force_x = nx + hx;
            net_force_y = ny + hy;
        }
        double velocity_x = (info_node->velocity_x + timestep * net_force_x) * damping;
        double velocity_y = (info_node->velocity_y + timestep * net_force_y) * damping;
        info_node->velocity_x = velocity_x;
        info_node->velocity_y = velocity_y;
        double x = info_node->x + timestep * info_node->velocity_x;
        double y = info_node->y + timestep * info_node->velocity_y;
        info_node->x = x;
        info_node->y = y;       
        kinetic_energy = kinetic_energy + info_node->mass * (hypot(info_node->velocity_x, info_node->velocity_y));
     }
    return kinetic_energy;
}

void clads_graph_export_force_layout(clads_graph_type *g,char* filename){
    FILE *file = fopen(filename, "w");
    if(file != NULL)
    {    
        clads_list_node_type *l = g->l_node->head;
        clads_graph_node_type *n;
        force_layout_node_information *a;
        while (l != NULL)
        {   
            n = CLADS_CAST(l->info,clads_graph_node_type*);
            a = CLADS_CAST(n->internal_info,force_layout_node_information*);
            fprintf(file,"%llu %Lf %Lf\n",n->id,a->x,a->y);
            l = l->next;
        }
        l = g->l_edge->head;
        clads_graph_edge_type *e;
        fprintf(file,"#\n");
        while (l != NULL)
        {   
            e = CLADS_CAST(l->info,clads_graph_edge_type*);
            fprintf(file,"%llu %llu\n",e->na->id,e->nb->id);
            l = l->next;
        }
        fclose(file);
    }
   else
   {
      perror(filename);
   }
}

void clads_graph_force_layout(force_layout_run_info *f,int n,double e){
    int x;
    double previous_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
    double kinetic_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
    do{
        previous_energy = kinetic_energy;
        for(x=0;x<n;x++){
            kinetic_energy  = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
        }
        printf("previous: %f, now: %f\n",previous_energy,kinetic_energy);
    }while(kinetic_energy<(previous_energy-(previous_energy*e)));
}

void clads_graph_force_layout_with_statistics(force_layout_run_info *f,int n,double e){
    FILE *file = fopen("statistics.txt", "w");
    if(file != NULL)
    {    
        int x;
        unsigned int steps = 0;
        double previous_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
        double kinetic_energy = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
        clock_t start;
        clock_t end;
        double elapsed_time = 0.0;
        fprintf(file,"# %i\n",n);
        do{
            previous_energy = kinetic_energy;
            for(x=0;x<n;x++){
                start = clock();
                kinetic_energy  = clads_graph_apply_forces(f->graph,f->attraction,f->repulsion,f->distanciaSpring,f->timestep,f->damping);
                end = clock();
                elapsed_time += (end-start)/(float)CLOCKS_PER_SEC;
                fprintf(file,"%u %f %f %f %f %f\n",steps,kinetic_energy,elapsed_time,
                    clads_graph_get_force_layout_aspect_ratio(f->graph),
                    clads_graph_get_force_layout_area(f->graph),
                    clads_graph_force_layout_lenght_edges(f->graph));
                steps++;
                printf("%d\n",steps);
            }
            printf("previous: %f, now: %f\n",previous_energy,kinetic_energy);
        }while(kinetic_energy<(previous_energy-(previous_energy*e)));
        fclose(file);
    }
   else
   {
      perror("statistics.txt");
   }
}

from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy as sp
import numpy as np
import math


if __name__ == '__main__':
    data = open("radial.txt", 'r').read().split('\n')
    args = data.pop(0).split(" ")
    width = int(float(args[0]))
    height = int(float(args[1]))
    ring_radius = float(args[2])
    number_of_rings = int(float(args[3]))
    screen_center = (height/2,width/2)
    raw_nodes = data[:data.index('#')]
    raw_edges = data[data.index('#')+1:-1]

    args_nodes = [x.split(" ") for x in raw_nodes]
    nodes = {args[0]: (float(args[1]),float(args[2])) for args in args_nodes}
   
    args_edges = [x.split(" ") for x in raw_edges]
    edges = [(args[0],args[1]) for args in args_edges]


    img = Image.new('RGB',(width+1, height+1),(255,255,255))
    d = ImageDraw.Draw(img)

   
    for x in range(1,number_of_rings+1):
        d.arc((screen_center[0]-(ring_radius*x), screen_center[1]-(ring_radius*x),
         screen_center[0]+(ring_radius*x), screen_center[1]+(ring_radius*x)),0,360,fill=(0,255,0))

    for edge in edges:
        d.line((nodes[edge[0]],nodes[edge[1]]), fill=(0,0,0), width=1)
    
    r = 5
    for key,value in nodes.items():
        d.ellipse((value[0]-r, value[1]-r, value[0]+r, value[1]+r), fill=(0,0,0))
    img.save("grafo.jpeg",'JPEG')

    '''
    header = open("statistics.txt", 'r').read().split('\n')[0]
    size_tick = int(header.split(' ')[1])

    data_statistics = sp.genfromtxt("statistics.txt", delimiter=" ")
    x = data_statistics[:,0]
    kinetic_energy = data_statistics[:,1]
    time = data_statistics[:,2]
    aspect_ratio = data_statistics[:,3]
    area = data_statistics[:,4]
    edges_lenght = data_statistics[:,5]

    dimension = (2,3)

    fig = plt.subplot2grid(dimension,(0, 0))

    plt.plot(x,kinetic_energy)
    plt.xlabel("Step")
    plt.ylabel("Kinetic energy")
    plt.grid(True, linestyle='-', color='0.75')
    number_ticks = int(len(data_statistics)/size_tick)
    plt.xticks([w*150 for w in range(number_ticks)],
        ['%i' % w for w in range(number_ticks)])
    plt.fill_between(x,kinetic_energy,0,color='blue')

    plt.subplot2grid(dimension,(0, 1))
    plt.plot(x,time)
    plt.xlabel("Step")
    plt.ylabel("Time")
    plt.grid(True, linestyle='-', color='0.75')
    plt.xticks([w*150 for w in range(number_ticks)],
        ['%i' % w for w in range(number_ticks)])
    plt.fill_between(x,time,0,color='blue')

    plt.subplot2grid(dimension,(0, 2))
    plt.plot(x,edges_lenght)
    plt.xlabel("Step")
    plt.ylabel("Edges lenght")
    plt.grid(True, linestyle='-', color='0.75')
    plt.xticks([w*150 for w in range(number_ticks)],
        ['%i' % w for w in range(number_ticks)])
    plt.fill_between(x,edges_lenght,0,color='blue')

  

    plt.subplot2grid(dimension,(1, 0))
    plt.plot(x,aspect_ratio)
    plt.xlabel("Step")
    plt.ylabel("Aspect ratio")
    plt.grid(True, linestyle='-', color='0.75')
    plt.xticks([w*150 for w in range(number_ticks)],
        ['%i' % w for w in range(number_ticks)])
    plt.fill_between(x,aspect_ratio,0,color='blue')

    plt.subplot2grid(dimension,(1, 1))
    plt.plot(x,area)
    plt.xlabel("Step")
    plt.ylabel("Area")
    plt.grid(True, linestyle='-', color='0.75')
    plt.xticks([w*150 for w in range(number_ticks)],
        ['%i' % w for w in range(number_ticks)])
    plt.fill_between(x,area,0,color='blue')

 
    #plt.show()
    '''