/**
 * Copyright (C) 2011 Joao Paulo de Souza Medeiros
 *
 * Author(s): Joao Paulo de Souza Medeiros <ignotus21@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#include "../code/clads.h"
#include "../code/graph.h"
#include <unistd.h>
#include <string.h>

void
graph_print(clads_graph_type *g)
{
    printf("nodes\n");
    clads_list_node_type *l = g->l_node->head;
    clads_graph_node_type *n;
    char* label;
    while (l != NULL)
    {   
        n = CLADS_CAST(l->info,clads_graph_node_type*);
        label = CLADS_CAST(n->info,char*);
        printf("id %llu, degree %lld, label: %s\n",n->id,n->degree,label);
        l = l->next;
    }
    printf("edges\n");
    l = g->l_edge->head;
    clads_graph_edge_type *e;
    while (l != NULL)
    {   
        e = CLADS_CAST(l->info,clads_graph_edge_type*);
        printf("%llu --> %llu\n",e->na->id,e->nb->id);
        l = l->next;
    }
}

void measure(){

    int n;
    int m;
    int i;
    char filename[50];
    char filename_radial[50];
    char filename_forces[50];
    clads_graph_type g;
    double node_radius = 5.0;
    int tree_depth;
    double width = 500;
    double height = 500;

    double base_radius = 0;
    FILE *file_mean = fopen("results/mean_measurements", "w");
    fprintf(file_mean,"area;total_edge_lenght;max_edge_lenght;angular_resolution;edge_crossing;aspect_ratio;computing_time;n;m;algorithm\n");
    FILE *file_real = fopen("results/real_measurements", "w");
    fprintf(file_real,"area;total_edge_lenght;max_edge_lenght;angular_resolution;edge_crossing;aspect_ratio;computing_time;n;m;i;algorithm\n");
    force_layout_run_info f;
    f.kinetic_energy = 0.0;
    f.steps = 0;
    f.elapsed_time = 0.0;
    f.attraction = 2.0;
    f.repulsion = 80000.0;
    f.distanciaSpring = 15;
    f.timestep  =  0.3;
    f.damping  =  0.15;
    int epoch = 50;
    double e = 0.1;
    int min_n = 50;
    int max_n = 150;
    int size_step_n = 50;
    int min_m = 1;
    int max_m = 3;
    int qtd_graphs = 2;
    int size_step_m = 1;
    double computing_time;

    double width_radial = 0;
    double height_radial = 0;

    double area;
    double total_edge_lenght;
    double max_edge_lenght;
    double angular_resolution;
    int edge_crossing;
    double aspect_ratio;

    double f_sum_area;
    double f_sum_total_edge_lenght;
    double f_sum_max_edge_lenght;
    double f_sum_angular_resolution;
    int f_sum_edge_crossing;
    double f_sum_aspect_ratio;
    double f_sum_computing_time;

    double r_sum_area;
    double r_sum_total_edge_lenght;
    double r_sum_max_edge_lenght;
    double r_sum_angular_resolution;
    int r_sum_edge_crossing;
    double r_sum_aspect_ratio;
    double r_sum_computing_time;

    for (n = min_n; n <= max_n; n+=size_step_n){
        for (m = min_m; m <= max_m; m+=size_step_m){
    
            f_sum_area = 0;
            f_sum_total_edge_lenght = 0;
            f_sum_max_edge_lenght = 0;
            f_sum_angular_resolution = 0;
            f_sum_edge_crossing = 0;
            f_sum_aspect_ratio = 0;
            f_sum_computing_time = 0;
          
            r_sum_area = 0;
            r_sum_total_edge_lenght = 0;
            r_sum_max_edge_lenght = 0;
            r_sum_angular_resolution = 0;
            r_sum_edge_crossing = 0;
            r_sum_aspect_ratio = 0;
            r_sum_computing_time = 0;

            for (i = 1;i <=qtd_graphs;i++){
                sprintf(filename, "graphs/n_%d-m_%d-g_%d.txt",n,m,i);
                printf("%s\n",filename);
                clads_graph_initialize_from_tgf_file(&g,filename);

                clads_graph_initialize_force_layout(&g,500,500);
                f.graph = &g;
                computing_time = 0;

                clads_graph_force_layout(&f,epoch,e,&computing_time);
                f_sum_computing_time += computing_time;
                printf("---force layout---\n");
                
                area = clads_graph_get_area(&g);
                f_sum_area += area;
                total_edge_lenght = clads_graph_lenght_edges(&g);
                f_sum_total_edge_lenght += total_edge_lenght;
                max_edge_lenght = clads_graph_max_edge(&g);
                f_sum_max_edge_lenght +=  max_edge_lenght;
                angular_resolution = clads_graph_get_smallest_angle(&g);
                f_sum_angular_resolution += angular_resolution;
                edge_crossing = clads_get_line_crossings(&g);
                f_sum_edge_crossing += edge_crossing;
                aspect_ratio = clads_graph_get_aspect_ratio(&g);
                f_sum_aspect_ratio += aspect_ratio;

                fprintf(file_real,"%f;%f;%f;%f;%d;%f;%f;%d;%d;%d;0\n",area,
                        total_edge_lenght,
                        max_edge_lenght,
                        angular_resolution,
                        edge_crossing,
                        aspect_ratio,
                        computing_time,
                        n,
                        m,
                        i);

                printf("    area: %f\n", area);
                printf("    total edge lenght: %f\n",total_edge_lenght);
                printf("    maximum edge lenght: %f\n",max_edge_lenght);
                printf("    angular resolution: %f\n",angular_resolution);
                printf("    edge crossing: %d\n",edge_crossing);
                printf("    aspect ratio: %f\n",aspect_ratio);
                printf("    time: %f\n",computing_time);

                clads_graph_adjust_to_screen(f.graph,width,height,50);
                sprintf(filename_forces, "results/graphs/n_%d-m_%d-g_%d-f.txt",n,m,i);
                clads_graph_export_force_layout(f.graph,filename_forces);
                clads_graph_finalize_force_layout(&g);
                
                computing_time = 0;
                tree_depth = clads_graph_initialize_radial_layout(&g,node_radius);
                
                clads_graph_radial_layout(&g,tree_depth,&width_radial,&height_radial,&base_radius,&computing_time);
                r_sum_computing_time += computing_time;
                area = M_PI*pow((base_radius*tree_depth),2);
                r_sum_area += area;
                aspect_ratio  = width_radial/height_radial;
                r_sum_aspect_ratio += aspect_ratio;

                total_edge_lenght = clads_graph_lenght_edges(&g);
                r_sum_total_edge_lenght +=  total_edge_lenght;
                max_edge_lenght = clads_graph_max_edge(&g);
                r_sum_max_edge_lenght += max_edge_lenght;
                angular_resolution = clads_graph_get_smallest_angle(&g);
                r_sum_angular_resolution +=  angular_resolution;
                edge_crossing = clads_get_line_crossings(&g);
                r_sum_edge_crossing += edge_crossing ;
                fprintf(file_real,"%f;%f;%f;%f;%d;%f;%f;%d;%d;%d;1\n",area,
                        total_edge_lenght,
                        max_edge_lenght,
                        angular_resolution,
                        edge_crossing,
                        aspect_ratio,
                        computing_time,
                        n,
                        m,
                        i);

                printf("---radial layout---\n");
                printf("    width: %f\n",width_radial);
                printf("    height: %f\n",height_radial);
                printf("    base_radius: %f\n",base_radius);
                printf("    area: %f\n",area);
                printf("    total edge lenght: %f\n",total_edge_lenght);
                printf("    maximum edge lenght: %f\n", max_edge_lenght);
                printf("    angular resolution: %f\n",angular_resolution);
                printf("    edge crossing: %d\n",edge_crossing);
                printf("    aspect ratio: %f\n",aspect_ratio);
                printf("    time: %f\n",computing_time);

                sprintf(filename_radial, "results/graphs/n_%d-m_%d-g_%d-r.txt",n,m,i);
                clads_graph_export_radial_layout(&g,filename_radial,width_radial,height_radial,base_radius,tree_depth,node_radius);
                clads_graph_finalize_radial_layout(&g);
                
                clads_graph_finalize(&g);
            }

            fprintf(file_mean,"%f;%f;%f;%f;%d;%f;%f;%d;%d;0\n",(f_sum_area/qtd_graphs),
                        (f_sum_total_edge_lenght/qtd_graphs),
                        (f_sum_max_edge_lenght/qtd_graphs),
                        (f_sum_angular_resolution/qtd_graphs),
                        (f_sum_edge_crossing/qtd_graphs),
                        (f_sum_aspect_ratio/qtd_graphs),
                        (f_sum_computing_time/qtd_graphs),
                        n,
                        m);

            fprintf(file_mean,"%f;%f;%f;%f;%d;%f;%f;%d;%d;1\n",(r_sum_area/qtd_graphs),
                        (r_sum_total_edge_lenght/qtd_graphs),
                        (r_sum_max_edge_lenght/qtd_graphs),
                        (r_sum_angular_resolution/qtd_graphs),
                        (r_sum_edge_crossing/qtd_graphs),
                        (r_sum_aspect_ratio/qtd_graphs),
                        (r_sum_computing_time/qtd_graphs),
                        n,
                        m);
        }
    }
    fclose(file_real);
    fclose(file_mean);
  }

int main( int   argc,
          char *argv[] )
{
    clads_initialize();
    measure();
    clads_finalize();

    /**
    clads_initialize();
    clads_graph_type g;
    clads_graph_initialize_from_tgf_file(&g,"graphs_data/n_150_m_1.txt");
    double width = 1000;
    double height = 1000;
    double node_radius = 8.0;
    int tree_depth = clads_graph_initialize_radial_layout(&g,node_radius);
    clads_graph_radial_layout(&g,width,height,tree_depth);
    printf("radial layout crossings: %d\n",clads_get_line_crossings(&g));
    clads_graph_finalize_radial_layout(&g);
    clads_graph_finalize(&g);
    clads_finalize();
    */
   
    return 0;
}

