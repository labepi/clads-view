from PIL import Image, ImageDraw, ImageFont
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy as sp
import numpy as np
import argparse
import argparse
import random

def plot_radial(filename):
    data = open(filename, 'r').read().split('\n')
    args = data.pop(0).split(" ")
    width = int(float(args[0]))
    height = int(float(args[1]))
    ring_radius = float(args[2])
    number_of_rings = int(float(args[3]))
    r = int(float(args[4]))
    screen_center = (height/2,width/2)
    raw_nodes = data[:data.index('#')]
    raw_edges = data[data.index('#')+1:-1]

    args_nodes = [x.split(" ") for x in raw_nodes]
    nodes = {args[0]: (float(args[1]),float(args[2])) for args in args_nodes}
       
    args_edges = [x.split(" ") for x in raw_edges]
    edges = [(args[0],args[1]) for args in args_edges]



    img = Image.new('RGB',(width+1, height+1),(255,255,255))
    d = ImageDraw.Draw(img)


    font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeMono.ttf", (r*2)-1)

    for x in range(1,number_of_rings+1):
        d.arc((screen_center[0]-(ring_radius*x), screen_center[1]-(ring_radius*x),
            screen_center[0]+(ring_radius*x), screen_center[1]+(ring_radius*x)),0,360,fill=(0,255,0))

    for edge in edges:
        d.line((nodes[edge[0]],nodes[edge[1]]), fill=(0,0,0), width=1)
        
    for key,value in nodes.items():
        if key == "0":
            d.ellipse((value[0]-r, value[1]-r, value[0]+r, value[1]+r), fill=(75,75,75))
        else:
            d.ellipse((value[0]-r, value[1]-r, value[0]+r, value[1]+r), fill=(random.randint(0,255),random.randint(0,255),random.randint(0,255)))
        d.text((value[0]-r, value[1]-r),key,(255,255,255),font=font)
    return img

def plot_force_layout(filename):
    data = open(filename, 'r').read().split('\n')
    raw_nodes = data[:data.index('#')]
    raw_edges = data[data.index('#')+1:-1]

    args_nodes = [x.split(" ") for x in raw_nodes]
    nodes = {args[0]: (float(args[1]),float(args[2])) for args in args_nodes}
    
    args_edges = [x.split(" ") for x in raw_edges]
    edges = [(args[0],args[1]) for args in args_edges]


    img = Image.new('RGB',(530, 530),(255,255,255))
    d = ImageDraw.Draw(img)
    for edge in edges:
        d.line((nodes[edge[0]],nodes[edge[1]]), fill=(0,0,0), width=1)
    r = 1
    for key,value in nodes.items():
        d.ellipse((value[0]-r, value[1]-r, value[0]+r, value[1]+r), fill=(0,0,0))
    return img;

if __name__ == '__main__':
    
    '''
    parser = argparse.ArgumentParser()

    parser.add_argument('-l',choices = (1,2), type=int, default = 1,
        help="algorithm used. 1 = radial, 2 = force layout. default = 1")
    args = parser.parse_args()
    layout =  args.l

    if (layout == 1):
         img = plot_radial("data/radial.txt")
         img.save("data/radial.jpeg",'JPEG')

    if (layout == 2):
        img = plot_force_layout("data/forces.txt");
        img.save("data/force.jpeg",'JPEG')   
    '''
    size_step = 50
    min_n = 50
    max_n = 150
    quantity = 2
    min_m = 1
    max_m = 3
    for n in range(min_n,max_n+1,size_step):
        for m in range(min_m,max_m+1):
            for g in range(1,quantity+1):   
                filename_forces = "results/graphs/n_{0}-m_{1}-g_{2}-f.txt".format(n,m,g)
                image_name_forces = "results/images/n_{0}-m_{1}-g_{2}-f.jpeg".format(n,m,g)
                print(filename_forces)
                img = plot_force_layout(filename_forces);
                img.save(image_name_forces,'JPEG') 
                filename_radial = "results/graphs/n_{0}-m_{1}-g_{2}-r.txt".format(n,m,g)
                image_name_radial = "results/images/n_{0}-m_{1}-g_{2}-r.jpeg".format(n,m,g) 
                print(filename_radial)
                img = plot_radial(filename_radial);
                img.save(image_name_radial,'JPEG') 
